﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EditShare
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Previsualisation = New System.Windows.Forms.PictureBox()
        Me.lblClose = New System.Windows.Forms.Label()
        Me.lblDelete = New System.Windows.Forms.Label()
        Me.lblTitreFenetre = New System.Windows.Forms.Label()
        Me.lblEdit = New System.Windows.Forms.Label()
        Me.lblShare = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.Previsualisation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Previsualisation)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(414, 244)
        Me.Panel1.TabIndex = 0
        '
        'Previsualisation
        '
        Me.Previsualisation.Location = New System.Drawing.Point(0, 0)
        Me.Previsualisation.Name = "Previsualisation"
        Me.Previsualisation.Size = New System.Drawing.Size(414, 244)
        Me.Previsualisation.TabIndex = 0
        Me.Previsualisation.TabStop = False
        '
        'lblClose
        '
        Me.lblClose.AutoSize = True
        Me.lblClose.Font = New System.Drawing.Font("Segoe WP", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClose.ForeColor = System.Drawing.Color.White
        Me.lblClose.Location = New System.Drawing.Point(596, 144)
        Me.lblClose.Name = "lblClose"
        Me.lblClose.Size = New System.Drawing.Size(69, 28)
        Me.lblClose.TabIndex = 4
        Me.lblClose.Text = "fermer"
        '
        'lblDelete
        '
        Me.lblDelete.AutoSize = True
        Me.lblDelete.Font = New System.Drawing.Font("Segoe WP", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDelete.ForeColor = System.Drawing.Color.White
        Me.lblDelete.Location = New System.Drawing.Point(564, 116)
        Me.lblDelete.Name = "lblDelete"
        Me.lblDelete.Size = New System.Drawing.Size(101, 28)
        Me.lblDelete.TabIndex = 5
        Me.lblDelete.Text = "supprimer"
        '
        'lblTitreFenetre
        '
        Me.lblTitreFenetre.AutoSize = True
        Me.lblTitreFenetre.Font = New System.Drawing.Font("Segoe WP", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitreFenetre.ForeColor = System.Drawing.Color.White
        Me.lblTitreFenetre.Location = New System.Drawing.Point(485, 9)
        Me.lblTitreFenetre.Name = "lblTitreFenetre"
        Me.lblTitreFenetre.Size = New System.Drawing.Size(180, 32)
        Me.lblTitreFenetre.TabIndex = 6
        Me.lblTitreFenetre.Text = "capture d'écran"
        '
        'lblEdit
        '
        Me.lblEdit.AutoSize = True
        Me.lblEdit.Font = New System.Drawing.Font("Segoe WP", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEdit.ForeColor = System.Drawing.Color.White
        Me.lblEdit.Location = New System.Drawing.Point(602, 60)
        Me.lblEdit.Name = "lblEdit"
        Me.lblEdit.Size = New System.Drawing.Size(63, 28)
        Me.lblEdit.TabIndex = 7
        Me.lblEdit.Text = "editer"
        '
        'lblShare
        '
        Me.lblShare.AutoSize = True
        Me.lblShare.Font = New System.Drawing.Font("Segoe WP", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShare.ForeColor = System.Drawing.Color.White
        Me.lblShare.Location = New System.Drawing.Point(577, 88)
        Me.lblShare.Name = "lblShare"
        Me.lblShare.Size = New System.Drawing.Size(88, 28)
        Me.lblShare.TabIndex = 7
        Me.lblShare.Text = "partager"
        '
        'EditShare
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(677, 245)
        Me.Controls.Add(Me.lblShare)
        Me.Controls.Add(Me.lblEdit)
        Me.Controls.Add(Me.lblClose)
        Me.Controls.Add(Me.lblDelete)
        Me.Controls.Add(Me.lblTitreFenetre)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "EditShare"
        Me.Text = "EditShare"
        Me.Panel1.ResumeLayout(False)
        CType(Me.Previsualisation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Previsualisation As System.Windows.Forms.PictureBox
    Friend WithEvents lblClose As System.Windows.Forms.Label
    Friend WithEvents lblDelete As System.Windows.Forms.Label
    Friend WithEvents lblTitreFenetre As System.Windows.Forms.Label
    Friend WithEvents lblEdit As System.Windows.Forms.Label
    Friend WithEvents lblShare As System.Windows.Forms.Label
End Class
