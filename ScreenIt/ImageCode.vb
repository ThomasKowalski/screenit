﻿Imports System.Drawing.Imaging

Module ImageCode
    Public Sub VaryQualityLevel()
        ' Get a bitmap.
        Dim bmp1 As New Bitmap(TempString)
        Dim jgpEncoder As ImageCodecInfo = GetEncoder(ImageFormat.Png)

        ' Create an Encoder object based on the GUID
        ' for the Quality parameter category.
        Dim myEncoder As System.Drawing.Imaging.Encoder = System.Drawing.Imaging.Encoder.Quality

        ' Create an EncoderParameters object.
        ' An EncoderParameters object has an array of EncoderParameter
        ' objects. In this case, there is only one
        ' EncoderParameter object in the array.
        Dim myEncoderParameters As New EncoderParameters(1)

        Dim myEncoderParameter As New EncoderParameter(myEncoder, 100&)
        myEncoderParameters.Param(0) = myEncoderParameter
        bmp1.Save(RepertoireTemporaire & "\compressed.jpg", jgpEncoder, myEncoderParameters)
        'myEncoderParameters.Param(0) = myEncoderParameter
        'bmp1.Save("D:\Users\Thomas\mega\TestPhotoQualityFifty.jpg", jgpEncoder, myEncoderParameters)

        'myEncoderParameter = New EncoderParameter(myEncoder, 100&)

        '' Save the bitmap as a JPG file with zero quality level compression.
        'myEncoderParameter = New EncoderParameter(myEncoder, 0&)
        'myEncoderParameters.Param(0) = myEncoderParameter
        'bmp1.Save("D:\Users\Thomas\mega\TestPhotoQualityZero.jpg", jgpEncoder, myEncoderParameters)

    End Sub 'VaryQualityLevel
    Private Function GetEncoder(ByVal format As ImageFormat) As ImageCodecInfo

        Dim codecs As ImageCodecInfo() = ImageCodecInfo.GetImageDecoders()

        Dim codec As ImageCodecInfo
        For Each codec In codecs
            If codec.FormatID = format.Guid Then
                Return codec
            End If
        Next codec
        Return Nothing

    End Function

End Module
