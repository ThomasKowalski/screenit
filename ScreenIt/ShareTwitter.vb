﻿Imports System.Net
Imports System.Collections.Specialized

Public Class ShareTwitter
    Dim NewPoint As New System.Drawing.Point()
    Dim X, Y As Integer
    Dim drag As Boolean
    Dim mousex As Integer
    Dim mousey As Integer
    Dim Success As Boolean

    Dim ResponseBody As String = ""

    Dim LoadedImage As Image

    Dim AdresseImage As String


#Region "Déplacement de la fenêtre"
    Private Sub Share_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseMove, lblSendTweet.MouseMove, PanelCapture.MouseMove, Previsualisation.MouseMove, lblTitlePage.MouseMove
        If e.Button = Windows.Forms.MouseButtons.Left Then
            If e.Button = MouseButtons.Left Then
                If drag Then
                    Me.Top = Windows.Forms.Cursor.Position.Y - mousey
                    Me.Left = Windows.Forms.Cursor.Position.X - mousex
                End If

            End If
        End If
    End Sub
    Private Sub Affichage_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseDown, lblSendTweet.MouseDown, PanelCapture.MouseDown, Previsualisation.MouseDown, lblTitlePage.MouseDown
        drag = True
        mousex = Windows.Forms.Cursor.Position.X - Me.Left
        mousey = Windows.Forms.Cursor.Position.Y - Me.Top
    End Sub
    Private Sub Affichage_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp, lblSendTweet.MouseUp, PanelCapture.MouseUp, Previsualisation.MouseUp, lblTitlePage.MouseUp
        drag = False
    End Sub
#End Region


    Private Sub LoadImage(path As String)
        loadedImage = Image.FromFile(path)
        Previsualisation.Image = LoadedImage
        PanelCapture.Width = LoadedImage.Width
        PanelCapture.Height = LoadedImage.Height
        Previsualisation.Left = 0
        Previsualisation.Top = 0
    End Sub


    Private Sub Share_Load(sender As Object, e As EventArgs) Handles Me.Load
        AdresseImage = RepertoireTemporaire & "\temp" & PublicCounter & "." & FormatSauvegarde
        PublicCounter += 1
        My.Computer.FileSystem.CopyFile(TempString, AdresseImage)
        Previsualisation.Height = PanelCapture.Height
        Previsualisation.Width = PanelCapture.Width
        Previsualisation.SizeMode = PictureBoxSizeMode.StretchImage
        LoadImage(AdresseImage)
        VaryQualityLevel()
    End Sub

    Private Sub lblSendTweet_Click(sender As Object, e As EventArgs) Handles lblSendTweet.Click
        Me.Height = 110
        For Each Ctl In Me.Controls
            If Ctl.Name <> "lblTitlePage" Then
                Ctl.visible = False
            End If
        Next
        ProgressBarAvancement.Visible = True
        BackgroundWorkerSendTweet.RunWorkerAsync()
    End Sub

    Private Sub BackgroundWorkerSendTweet_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorkerSendTweet.DoWork
        If AccessToken = "" Or AccessTokenSecret = "" Or ConsumerKey = "" Or ConsumerSecret = "" Then
            MessageBox.Show("Vous devez entrer vos clés Twitter avant de pouvoir envoyer des captures d'écran. Voulez vous le faire maintenant ?", "Envoi impossible.", MessageBoxButtons.YesNo, MessageBoxIcon.Error)
            Exit Sub
        End If
        Using client As New Net.WebClient
            Dim UploadResult As Byte() = client.UploadFile("http://thomaskowalski.net/twitterapi/upload-image.php", RepertoireTemporaire & "\compressed.jpg")
            Dim UploadedImageName As String = Split(ASCIIBytesToString(UploadResult), ";")(2)
            Dim reqparm As New Specialized.NameValueCollection
            reqparm.Add("consumerkey", ConsumerKey)
            reqparm.Add("consumersecret", ConsumerSecret)
            reqparm.Add("accesstoken", AccessToken)
            reqparm.Add("accesstokensecret", AccessTokenSecret)
            reqparm.Add("tweet", tbContenuTweet.Text)
            reqparm.Add("image", UploadedImageName)
            Dim responsebytes = client.UploadValues("http://thomaskowalski.net/twitterapi/tweet-with-image.php", "POST", reqparm)
            responsebody = (New System.Text.UTF8Encoding).GetString(responsebytes)
        End Using
    End Sub

    Private Sub BackgroundWorkerSendTweet_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorkerSendTweet.RunWorkerCompleted
        If ResponseBody.Split(";")(0) = "success" Then
            LoadedImage.Dispose()
            Me.Close()
        ElseIf ResponseBody.Split(";")(1) = "2" Then
            Me.Hide()
            MessageBox.Show("Désolé mais les clés que vous avez entrées sont incorrectes. Veuillez vérifier leur validité sur apps.twitter.com", "Données de connexion invalides.", MessageBoxButtons.OK, MessageBoxIcon.Error)
            LoadedImage.Dispose()
            Me.Close()
        Else
            MessageBox.Show("Désolé, mais une erreur est survenue lors de l'envoi de votre capture d'écran à Twitter. Veuillez réessayer ultérieurement.", "Impossible d'envoyer le tweet.", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
End Class