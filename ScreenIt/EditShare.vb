﻿Public Class EditShare
    Private ImageLocation = TempString
    Private Sub EditShare_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim Image As Bitmap = Bitmap.FromFile(ImageLocation)
        Previsualisation.Image = Image
        Previsualisation.SizeMode = PictureBoxSizeMode.StretchImage
        Panel1.Width = CInt(Image.Width / 2.5)
        Panel1.Height = CInt(Image.Height / 2.5)
        Previsualisation.Width = Panel1.Width
        Previsualisation.Height = Panel1.Height
        Me.Width = Panel1.Width + 200
        Me.Height = Panel1.Height
        AddHandler lblEdit.Click, AddressOf Edit.Show
        For Each Ctl In Me.Controls
            If Ctl.GetType.ToString = "System.Windows.Forms.Label" Then
                Ctl.left = Me.Width - Ctl.width
                Debug.Print(Ctl.name)
            Else
            End If
        Next
    End Sub

    Private Sub lblClose_Click(sender As Object, e As EventArgs) Handles lblClose.Click
        Me.Close()
    End Sub

    Private Sub lblDelete_Click(sender As Object, e As EventArgs) Handles lblDelete.Click
        DeletingImage.Show()
        Me.Close()
    End Sub
End Class