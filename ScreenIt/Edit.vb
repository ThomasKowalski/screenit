﻿Class Edit
    Dim CropRectangle As Rectangle = Nothing
    Dim ImagePath As String = TempString
    Dim InitialCursorPosition As Point = Nothing

    Dim NewPoint As New System.Drawing.Point()
    Dim X, Y As Integer
    Dim drag As Boolean
    Dim mousex As Integer
    Dim mousey As Integer

    Dim NewPointPanel As New System.Drawing.Point()
    Dim XPanel, YPanel As Integer
    Dim dragPanel As Boolean
    Dim mousexPanel As Integer
    Dim mouseyPanel As Integer

    Dim CreatedRectangle As Boolean = False

    Dim ExportName As String = IO.Path.GetFileNameWithoutExtension(ImagePath)


#Region "Déplacement de la fenêtre"
    Private Sub Affichage_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseMove, PanelPictureBox.MouseMove, lblPartagerTwitter.MouseMove, lblRecommencer.MouseMove, lblSauvegarder.MouseMove, lblSupprimer.MouseMove, lblTitreFenetre.MouseMove, lblPartagerImgur.MouseMove
        If e.Button = Windows.Forms.MouseButtons.Left Then
            If e.Button = MouseButtons.Left Then
                If drag Then
                    Me.Top = Windows.Forms.Cursor.Position.Y - mousey
                    Me.Left = Windows.Forms.Cursor.Position.X - mousex
                End If

            End If
        End If
    End Sub
    Private Sub Affichage_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseDown, PanelPictureBox.MouseDown, lblPartagerTwitter.MouseDown, lblRecommencer.MouseDown, lblSauvegarder.MouseDown, lblSupprimer.MouseDown, lblTitreFenetre.MouseDown, lblPartagerImgur.MouseDown
        drag = True
        mousex = Windows.Forms.Cursor.Position.X - Me.Left
        mousey = Windows.Forms.Cursor.Position.Y - Me.Top
    End Sub
    Private Sub Affichage_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp, PanelPictureBox.MouseUp, lblPartagerTwitter.MouseUp, lblRecommencer.MouseUp, lblSauvegarder.MouseUp, lblSupprimer.MouseUp, lblTitreFenetre.MouseUp, lblPartagerImgur.MouseUp
        drag = False
    End Sub
#End Region

#Region "Déplacement du panel de commande"
    Private Sub PanelBoutons_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PanelBoutons.MouseMove
        If e.Button = Windows.Forms.MouseButtons.Left Then
            If e.Button = MouseButtons.Left Then
                If dragPanel Then
                    PanelBoutons.Top = Windows.Forms.Cursor.Position.Y - mouseyPanel
                    PanelBoutons.Left = Windows.Forms.Cursor.Position.X - mousexPanel
                End If

            End If
        End If
    End Sub
    Private Sub PanelBoutons_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PanelBoutons.MouseDown
        If Me.Size <> Screen.PrimaryScreen.Bounds.Size Then
            Exit Sub
        End If
        dragPanel = True
        mousexPanel = Windows.Forms.Cursor.Position.X - PanelBoutons.Left
        mouseyPanel = Windows.Forms.Cursor.Position.Y - PanelBoutons.Top
    End Sub
    Private Sub PanelBoutons_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PanelBoutons.MouseUp
        dragPanel = False
    End Sub
#End Region



    Private Sub LoadImage(path As String)
        Dim LoadedImage As Image = Image.FromFile(path)
        MainPictureBox.Image = LoadedImage
        Debug.Print(MainPictureBox.Size.ToString & "/" & LoadedImage.Size.ToString)
        PanelPictureBox.Size = LoadedImage.Size
        MainPictureBox.Left = 0
        MainPictureBox.Top = 0
        PanelPictureBox.Left = 0
        PanelPictureBox.Top = 0
        Me.TopMost = True
        Me.Left = 0
        Me.Top = 0
        Me.Height = PanelPictureBox.Height
        Me.Width = PanelPictureBox.Width
        PanelBoutons.Left = Me.Width - PanelBoutons.Width
    End Sub

    Private Sub Edit_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            LoadImage(ImagePath)
        Catch ex As Exception
            MessageBox.Show("ScreenIt! a rencontré un problème et n'est pas en mesure d'afficher la fenêtre d'édition. Merci de nous excuser du désagément.", "Erreur fatale.", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End Try
        lblNomCapture.Text = IO.Path.GetFileNameWithoutExtension(ImagePath)
        bValiderNomCapture.Height = bValiderNomCapture.Width = 0
    End Sub

    Private Sub MainPictureBox_MouseDown(sender As Object, e As MouseEventArgs) Handles MainPictureBox.MouseDown
        InitialCursorPosition = New Point(Cursor.Position.X - Me.Left - PanelPictureBox.Left, Cursor.Position.Y - Me.Top - PanelPictureBox.Top)
    End Sub

    Private Sub MainPictureBox_MouseMove(sender As Object, e As MouseEventArgs) Handles MainPictureBox.MouseMove
        If e.Button = Windows.Forms.MouseButtons.Left Then
            CreatedRectangle = True
            If Cursor.Position.Y >= Me.Top + PanelPictureBox.Top + PanelPictureBox.Height Or Cursor.Position.X >= Me.Left + PanelPictureBox.Left + PanelPictureBox.Width Then
                Exit Sub
            End If
            CropRectangle = New Rectangle(InitialCursorPosition.X, InitialCursorPosition.Y, Cursor.Position.X - InitialCursorPosition.X - Me.Left - PanelPictureBox.Left, Cursor.Position.Y - InitialCursorPosition.Y - Me.Top - PanelPictureBox.Top)
            Dim myPen As New Pen(Brushes.Red, 1)
            MainPictureBox.Refresh()
            MainPictureBox.CreateGraphics.DrawRectangle(myPen, CropRectangle)
        End If
    End Sub

    Private Sub MainPictureBox_MouseUp(sender As Object, e As MouseEventArgs) Handles MainPictureBox.MouseUp
        If CreatedRectangle = False Then
            Exit Sub
        Else
            CreatedRectangle = False
        End If
        Dim _img = getCroppedImage()
        MainPictureBox.Image = _img
        PanelPictureBox.Size = MainPictureBox.Size
        If PanelPictureBox.Height < PanelBoutons.Height Then
            Me.Height = PanelBoutons.Height
            PanelPictureBox.Top = (Me.Height - PanelPictureBox.Height) / 2
        Else
            Me.Height = PanelPictureBox.Height
        End If
        If PanelPictureBox.Width > Screen.PrimaryScreen.Bounds.Width - PanelBoutons.Width Then
            Me.Left = 0
            Me.Width = Screen.PrimaryScreen.Bounds.Width
            PanelPictureBox.Left = 0
        Else
            Me.Width = PanelPictureBox.Width + PanelBoutons.Width
        End If
        PanelBoutons.Left = Me.Width - PanelBoutons.Width
    End Sub

    Function getCroppedImage() As Bitmap
        Dim rect As Rectangle = New Rectangle(InitialCursorPosition.X, InitialCursorPosition.Y, CropRectangle.Width, CropRectangle.Height)
        Dim OriginalImage As Bitmap = New Bitmap(MainPictureBox.Image, MainPictureBox.Width, MainPictureBox.Height)
        If CropRectangle.Width < 1 Or CropRectangle.Height < 1 Then
            Return OriginalImage
            Exit Function
        End If
        Dim _img As New Bitmap(CropRectangle.Width, CropRectangle.Height)
        Dim g As Graphics = Graphics.FromImage(_img)
        g.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
        g.PixelOffsetMode = Drawing2D.PixelOffsetMode.HighQuality
        g.CompositingQuality = Drawing2D.CompositingQuality.HighQuality
        g.DrawImage(OriginalImage, 0, 0, rect, GraphicsUnit.Pixel)
        Return _img
    End Function

    Private Sub lblRecommencer_Click(sender As Object, e As EventArgs) Handles lblRecommencer.Click
        LoadImage(ImagePath)
    End Sub

    Private Sub lblSupprimer_Click(sender As Object, e As EventArgs) Handles lblSupprimer.Click
        Dim MessageBoxResult = MessageBox.Show("Voulez-vous vraiment supprimer cette capture d'écran ?", "Suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
        If MessageBoxResult = vbYes Then
            MainPictureBox.Image = Nothing
            TempString = ImagePath
            DeletingImage.Show()
            Me.Close()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub lblSauvegarder_Click(sender As Object, e As EventArgs) Handles lblSauvegarder.Click
        MainPictureBox.Image.Save(RepertoireSauvegarde & "\" & ExportName & "." & FormatSauvegarde, FormatSauvegardeImageFormat)
        MainPictureBox.Image.Dispose()
        Me.Close()
    End Sub

    Private Sub lblPartager_Click(sender As Object, e As EventArgs) Handles lblPartagerTwitter.Click
        TempString = RepertoireSauvegarde & "\" & IO.Path.GetFileName(ImagePath)
        MainPictureBox.Image.Save(RepertoireSauvegarde & "\" & ExportName & "." & FormatSauvegarde, FormatSauvegardeImageFormat)
        MainPictureBox.Image.Dispose()
        ShareTwitter.Show()
        Me.Close()
    End Sub

    Private Sub lblPartagerImgur_Click(sender As Object, e As EventArgs) Handles lblPartagerImgur.Click
        bwImgur.RunWorkerAsync()
    End Sub

    Dim responsebody As String

    Private Sub bwImgur_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bwImgur.DoWork
        MainPictureBox.Image.Save(RepertoireSauvegarde & "\" & IO.Path.GetFileName(ImagePath), FormatSauvegardeImageFormat)
        Debug.Print(RepertoireSauvegarde & "\" & IO.Path.GetFileName(ImagePath) & "." & FormatSauvegarde)
        Using client As New Net.WebClient
            Try
                Dim UploadResult As Byte() = client.UploadFile("http://thomaskowalski.net/twitterapi/upload-image.php", RepertoireSauvegarde & "\" & IO.Path.GetFileName(ImagePath))
                Dim UploadedImageName As String = Split(ASCIIBytesToString(UploadResult), ";")(2)
                Dim reqparm As New Specialized.NameValueCollection
                reqparm.Add("image", UploadedImageName)
                Dim responsebytes = client.UploadValues("http://thomaskowalski.net/twitterapi/upload-to-imgur.php", "POST", reqparm)
                responsebody = (New System.Text.UTF8Encoding).GetString(responsebytes)
            Catch ex As Exception
                MessageBox.Show("Désolé, mais une erreur est survenue lors de l'envoi de l'image.", "Erreur lors de l'envoi", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End Using
    End Sub

    Private Sub bwImgur_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bwImgur.RunWorkerCompleted
        If responsebody = "" Then Exit Sub
        Dim tableauResponse As String() = responsebody.Split(";")
        If tableauResponse(0) = "error" Then
            MessageBox.Show("Désolé, mais nous n'arrivons pas à envoyer la capture d'écran sur Imgur pour le moment. Merci de réessayer ultérieurement.", "Impossible de mettre en ligne.", MessageBoxButtons.OK, MessageBoxIcon.Error)
        ElseIf tableauResponse(0) = "success" Then
            lblPartagerImgur.Visible = False
            tbUrlImgur.Text = tableauResponse(2)
            tbUrlImgur.Visible = True
            tbUrlImgur.Focus()
        End If
    End Sub
    Private Sub tbUrlImgur_MouseDown(sender As Object, e As MouseEventArgs) Handles tbUrlImgur.MouseDown
        If e.Button = Windows.Forms.MouseButtons.Right Then
            My.Computer.Clipboard.SetText(tbUrlImgur.Text)
            MainPictureBox.Image.Dispose()
            Me.Close()
        End If
    End Sub
    Private Sub hideTbNomCapture(sender As Object, e As EventArgs) Handles lblNomCapture.Click, lblPartagerImgur.Click, lblPartagerTwitter.Click, lblRecommencer.Click, lblSauvegarder.Click, lblSauvegarder.Click, lblSupprimer.Click, lblTitreFenetre.Click, PanelBoutons.Click, PanelPictureBox.Click, MainPictureBox.Click
        If tbNomCapture.Text <> "" Then
            ExportName = tbNomCapture.Text
        End If
        tbNomCapture.Visible = False
        lblNomCapture.Text = ExportName
        lblNomCapture.Visible = True
    End Sub
    Private Sub lblNomCapture_Click(sender As Object, e As EventArgs) Handles lblNomCapture.Click
        tbNomCapture.Text = ExportName
        tbNomCapture.Visible = True
        lblNomCapture.Visible = False
        Me.AcceptButton = bValiderNomCapture
        tbNomCapture.Focus()
    End Sub

    Private Sub bValiderNomCapture_Click(sender As Object, e As EventArgs) Handles bValiderNomCapture.Click
        If tbNomCapture.Text <> "" Then
            ExportName = tbNomCapture.Text
        End If
        tbNomCapture.Visible = False
        lblNomCapture.Text = ExportName
        lblNomCapture.Visible = True
    End Sub
End Class