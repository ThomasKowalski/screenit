﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Principal
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Principal))
        Me.panelRepertoires = New System.Windows.Forms.Panel()
        Me.bValiderTemp = New System.Windows.Forms.Button()
        Me.bValiderAdresseHQ = New System.Windows.Forms.Button()
        Me.lblLocationTemp = New System.Windows.Forms.Label()
        Me.lblLocationHQ = New System.Windows.Forms.Label()
        Me.lblRepTemp = New System.Windows.Forms.Label()
        Me.lblRepHQ = New System.Windows.Forms.Label()
        Me.tbLocationTemp = New System.Windows.Forms.TextBox()
        Me.tbLocationHQ = New System.Windows.Forms.TextBox()
        Me.ongletRepertoires = New System.Windows.Forms.Label()
        Me.ongletQualite = New System.Windows.Forms.Label()
        Me.panelQualite = New System.Windows.Forms.Panel()
        Me.lblFormatBPG = New System.Windows.Forms.Label()
        Me.lblFilenameFormat = New System.Windows.Forms.Label()
        Me.tbNomFichier = New System.Windows.Forms.TextBox()
        Me.bValiderFilenameFormat = New System.Windows.Forms.Button()
        Me.lblFormatTIFF = New System.Windows.Forms.Label()
        Me.lblFormatGIF = New System.Windows.Forms.Label()
        Me.lblFormatBMP = New System.Windows.Forms.Label()
        Me.lblFormatPNG = New System.Windows.Forms.Label()
        Me.lblFormatJPG = New System.Windows.Forms.Label()
        Me.lblJokers3 = New System.Windows.Forms.Label()
        Me.lblJokers2 = New System.Windows.Forms.Label()
        Me.lblJokers = New System.Windows.Forms.Label()
        Me.lblResultat = New System.Windows.Forms.Label()
        Me.lblAutresParametres = New System.Windows.Forms.Label()
        Me.lblSelectionFormat = New System.Windows.Forms.Label()
        Me.ongletRaccourcis = New System.Windows.Forms.Label()
        Me.ongletPartage = New System.Windows.Forms.Label()
        Me.panelRaccourcis = New System.Windows.Forms.Panel()
        Me.cbHQ = New System.Windows.Forms.CheckBox()
        Me.lblRaccourciImgur = New System.Windows.Forms.Label()
        Me.lblraccourciEdit = New System.Windows.Forms.Label()
        Me.raccourciImgur = New System.Windows.Forms.Label()
        Me.lblraccourciTwitter = New System.Windows.Forms.Label()
        Me.raccourciEdit = New System.Windows.Forms.Label()
        Me.raccourciTwitter = New System.Windows.Forms.Label()
        Me.lblStopCapture = New System.Windows.Forms.Label()
        Me.tbContenuTweet = New System.Windows.Forms.TextBox()
        Me.raccourciScreenshot = New System.Windows.Forms.Label()
        Me.lblRaccourciScreenshot = New System.Windows.Forms.Label()
        Me.panelPartage = New System.Windows.Forms.Panel()
        Me.lblConsumerSecret = New System.Windows.Forms.Label()
        Me.tbConsumerSecret = New System.Windows.Forms.TextBox()
        Me.lblTitleCS = New System.Windows.Forms.Label()
        Me.lblConsumerKey = New System.Windows.Forms.Label()
        Me.tbConsumerKey = New System.Windows.Forms.TextBox()
        Me.lblTitleCK = New System.Windows.Forms.Label()
        Me.lblAccessTokenSecret = New System.Windows.Forms.Label()
        Me.lblInfosTwitter = New System.Windows.Forms.Label()
        Me.bValiderConsumerSecret = New System.Windows.Forms.Button()
        Me.bValiderAccessTokenSecret = New System.Windows.Forms.Button()
        Me.lblSendTweet = New System.Windows.Forms.Label()
        Me.lblTitleAT = New System.Windows.Forms.Label()
        Me.bValiderConsumerKey = New System.Windows.Forms.Button()
        Me.bValiderAccessToken = New System.Windows.Forms.Button()
        Me.lblAccessToken = New System.Windows.Forms.Label()
        Me.tbAccessToken = New System.Windows.Forms.TextBox()
        Me.tbAccessTokenSecret = New System.Windows.Forms.TextBox()
        Me.lblTitleATS = New System.Windows.Forms.Label()
        Me.TimerHotKeys = New System.Windows.Forms.Timer(Me.components)
        Me.TimerFilename = New System.Windows.Forms.Timer(Me.components)
        Me.IconeNotification = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.MenuNotification = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.LibrairieToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RéglagesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NotificationsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuitterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolTipAffichage = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblFermer = New System.Windows.Forms.Label()
        Me.panelRepertoires.SuspendLayout()
        Me.panelQualite.SuspendLayout()
        Me.panelRaccourcis.SuspendLayout()
        Me.panelPartage.SuspendLayout()
        Me.MenuNotification.SuspendLayout()
        Me.SuspendLayout()
        '
        'panelRepertoires
        '
        Me.panelRepertoires.Controls.Add(Me.bValiderTemp)
        Me.panelRepertoires.Controls.Add(Me.bValiderAdresseHQ)
        Me.panelRepertoires.Controls.Add(Me.lblLocationTemp)
        Me.panelRepertoires.Controls.Add(Me.lblLocationHQ)
        Me.panelRepertoires.Controls.Add(Me.lblRepTemp)
        Me.panelRepertoires.Controls.Add(Me.lblRepHQ)
        Me.panelRepertoires.Controls.Add(Me.tbLocationTemp)
        Me.panelRepertoires.Controls.Add(Me.tbLocationHQ)
        Me.panelRepertoires.Location = New System.Drawing.Point(17, 49)
        Me.panelRepertoires.Name = "panelRepertoires"
        Me.panelRepertoires.Size = New System.Drawing.Size(495, 260)
        Me.panelRepertoires.TabIndex = 2
        '
        'bValiderTemp
        '
        Me.bValiderTemp.Location = New System.Drawing.Point(5, 100)
        Me.bValiderTemp.Name = "bValiderTemp"
        Me.bValiderTemp.Size = New System.Drawing.Size(22, 23)
        Me.bValiderTemp.TabIndex = 7
        Me.bValiderTemp.Text = "Button1"
        Me.bValiderTemp.UseVisualStyleBackColor = True
        '
        'bValiderAdresseHQ
        '
        Me.bValiderAdresseHQ.Location = New System.Drawing.Point(5, 35)
        Me.bValiderAdresseHQ.Name = "bValiderAdresseHQ"
        Me.bValiderAdresseHQ.Size = New System.Drawing.Size(22, 23)
        Me.bValiderAdresseHQ.TabIndex = 7
        Me.bValiderAdresseHQ.Text = "Button1"
        Me.bValiderAdresseHQ.UseVisualStyleBackColor = True
        '
        'lblLocationTemp
        '
        Me.lblLocationTemp.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLocationTemp.ForeColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.lblLocationTemp.Location = New System.Drawing.Point(33, 93)
        Me.lblLocationTemp.Name = "lblLocationTemp"
        Me.lblLocationTemp.Size = New System.Drawing.Size(450, 20)
        Me.lblLocationTemp.TabIndex = 4
        Me.lblLocationTemp.Text = "%TEMP%"
        Me.ToolTipAffichage.SetToolTip(Me.lblLocationTemp, "Cliquez ici pour éditer l'adresse du répertoire temporaire.")
        '
        'lblLocationHQ
        '
        Me.lblLocationHQ.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLocationHQ.ForeColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.lblLocationHQ.Location = New System.Drawing.Point(33, 38)
        Me.lblLocationHQ.Name = "lblLocationHQ"
        Me.lblLocationHQ.Size = New System.Drawing.Size(450, 20)
        Me.lblLocationHQ.TabIndex = 4
        Me.lblLocationHQ.Text = "C:\Users\Thomas\Images\Captures"
        Me.ToolTipAffichage.SetToolTip(Me.lblLocationHQ, "Cliquez ici pour éditer l'adresse du dossier de sauvegarde.")
        '
        'lblRepTemp
        '
        Me.lblRepTemp.AutoSize = True
        Me.lblRepTemp.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRepTemp.ForeColor = System.Drawing.Color.White
        Me.lblRepTemp.Location = New System.Drawing.Point(9, 66)
        Me.lblRepTemp.Name = "lblRepTemp"
        Me.lblRepTemp.Size = New System.Drawing.Size(264, 20)
        Me.lblRepTemp.TabIndex = 2
        Me.lblRepTemp.Text = "répertoire de sauvegarde (temporaire)"
        '
        'lblRepHQ
        '
        Me.lblRepHQ.AutoSize = True
        Me.lblRepHQ.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRepHQ.ForeColor = System.Drawing.Color.White
        Me.lblRepHQ.Location = New System.Drawing.Point(9, 9)
        Me.lblRepHQ.Name = "lblRepHQ"
        Me.lblRepHQ.Size = New System.Drawing.Size(277, 20)
        Me.lblRepHQ.TabIndex = 2
        Me.lblRepHQ.Text = "répertoire de sauvegarde (haute qualité)"
        Me.ToolTipAffichage.SetToolTip(Me.lblRepHQ, "Cliquez ici pour ouvrir le dossier.")
        '
        'tbLocationTemp
        '
        Me.tbLocationTemp.Font = New System.Drawing.Font("Segoe WP", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLocationTemp.Location = New System.Drawing.Point(33, 90)
        Me.tbLocationTemp.Name = "tbLocationTemp"
        Me.tbLocationTemp.Size = New System.Drawing.Size(450, 27)
        Me.tbLocationTemp.TabIndex = 6
        '
        'tbLocationHQ
        '
        Me.tbLocationHQ.Font = New System.Drawing.Font("Segoe WP", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLocationHQ.Location = New System.Drawing.Point(33, 35)
        Me.tbLocationHQ.Name = "tbLocationHQ"
        Me.tbLocationHQ.Size = New System.Drawing.Size(446, 27)
        Me.tbLocationHQ.TabIndex = 6
        '
        'ongletRepertoires
        '
        Me.ongletRepertoires.AutoSize = True
        Me.ongletRepertoires.Font = New System.Drawing.Font("Segoe WP", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ongletRepertoires.ForeColor = System.Drawing.Color.White
        Me.ongletRepertoires.Location = New System.Drawing.Point(12, 9)
        Me.ongletRepertoires.Name = "ongletRepertoires"
        Me.ongletRepertoires.Size = New System.Drawing.Size(131, 32)
        Me.ongletRepertoires.TabIndex = 2
        Me.ongletRepertoires.Text = "répertoires"
        '
        'ongletQualite
        '
        Me.ongletQualite.AutoSize = True
        Me.ongletQualite.Font = New System.Drawing.Font("Segoe WP", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ongletQualite.ForeColor = System.Drawing.Color.Gray
        Me.ongletQualite.Location = New System.Drawing.Point(149, 9)
        Me.ongletQualite.Name = "ongletQualite"
        Me.ongletQualite.Size = New System.Drawing.Size(86, 32)
        Me.ongletQualite.TabIndex = 2
        Me.ongletQualite.Text = "format"
        '
        'panelQualite
        '
        Me.panelQualite.Controls.Add(Me.lblFormatBPG)
        Me.panelQualite.Controls.Add(Me.lblFilenameFormat)
        Me.panelQualite.Controls.Add(Me.tbNomFichier)
        Me.panelQualite.Controls.Add(Me.bValiderFilenameFormat)
        Me.panelQualite.Controls.Add(Me.lblFormatTIFF)
        Me.panelQualite.Controls.Add(Me.lblFormatGIF)
        Me.panelQualite.Controls.Add(Me.lblFormatBMP)
        Me.panelQualite.Controls.Add(Me.lblFormatPNG)
        Me.panelQualite.Controls.Add(Me.lblFormatJPG)
        Me.panelQualite.Controls.Add(Me.lblJokers3)
        Me.panelQualite.Controls.Add(Me.lblJokers2)
        Me.panelQualite.Controls.Add(Me.lblJokers)
        Me.panelQualite.Controls.Add(Me.lblResultat)
        Me.panelQualite.Controls.Add(Me.lblAutresParametres)
        Me.panelQualite.Controls.Add(Me.lblSelectionFormat)
        Me.panelQualite.Location = New System.Drawing.Point(557, 49)
        Me.panelQualite.Name = "panelQualite"
        Me.panelQualite.Size = New System.Drawing.Size(495, 260)
        Me.panelQualite.TabIndex = 5
        '
        'lblFormatBPG
        '
        Me.lblFormatBPG.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormatBPG.ForeColor = System.Drawing.Color.Gray
        Me.lblFormatBPG.Location = New System.Drawing.Point(333, 184)
        Me.lblFormatBPG.Name = "lblFormatBPG"
        Me.lblFormatBPG.Size = New System.Drawing.Size(54, 36)
        Me.lblFormatBPG.TabIndex = 9
        Me.lblFormatBPG.Text = "BPG"
        Me.lblFormatBPG.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ToolTipAffichage.SetToolTip(Me.lblFormatBPG, "Le format BPG est une évolution du JPG qui présente une excellente qualité pour u" & _
        "n poids plume. Cependant, il n'est que très peu utilisé pour le moment.")
        '
        'lblFilenameFormat
        '
        Me.lblFilenameFormat.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFilenameFormat.ForeColor = System.Drawing.Color.White
        Me.lblFilenameFormat.Location = New System.Drawing.Point(33, 36)
        Me.lblFilenameFormat.Name = "lblFilenameFormat"
        Me.lblFilenameFormat.Size = New System.Drawing.Size(450, 20)
        Me.lblFilenameFormat.TabIndex = 8
        Me.lblFilenameFormat.Text = "Capture d'écran du \y-\m-\d à \hh \mnmn et \ss"
        Me.ToolTipAffichage.SetToolTip(Me.lblFilenameFormat, "Vous pouvez ici personnaliser le nom de fichier par défaut de vos captures d'écra" & _
        "n. Vous pouvez utiliser les jokers ci-dessous qui seront automatiquement remplac" & _
        "és par leurs valeurs respectives.")
        '
        'tbNomFichier
        '
        Me.tbNomFichier.Font = New System.Drawing.Font("Segoe WP", 11.25!)
        Me.tbNomFichier.Location = New System.Drawing.Point(33, 33)
        Me.tbNomFichier.Name = "tbNomFichier"
        Me.tbNomFichier.Size = New System.Drawing.Size(450, 27)
        Me.tbNomFichier.TabIndex = 6
        Me.tbNomFichier.Text = "Capture d'écran du \y-\m-\d à \hh \mnmn et \ss"
        '
        'bValiderFilenameFormat
        '
        Me.bValiderFilenameFormat.Location = New System.Drawing.Point(5, 33)
        Me.bValiderFilenameFormat.Name = "bValiderFilenameFormat"
        Me.bValiderFilenameFormat.Size = New System.Drawing.Size(22, 23)
        Me.bValiderFilenameFormat.TabIndex = 7
        Me.bValiderFilenameFormat.Text = "Button1"
        Me.bValiderFilenameFormat.UseVisualStyleBackColor = True
        '
        'lblFormatTIFF
        '
        Me.lblFormatTIFF.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormatTIFF.ForeColor = System.Drawing.Color.Gray
        Me.lblFormatTIFF.Location = New System.Drawing.Point(273, 184)
        Me.lblFormatTIFF.Name = "lblFormatTIFF"
        Me.lblFormatTIFF.Size = New System.Drawing.Size(54, 36)
        Me.lblFormatTIFF.TabIndex = 5
        Me.lblFormatTIFF.Text = "TIFF"
        Me.lblFormatTIFF.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ToolTipAffichage.SetToolTip(Me.lblFormatTIFF, "Le format TIFF présente l'avantage d'être léger et de bonne qualité. Cependant, c" & _
        "'est un format peu répandu, notamment sur le Web, où il sera souvent non reconnu" & _
        ".")
        '
        'lblFormatGIF
        '
        Me.lblFormatGIF.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormatGIF.ForeColor = System.Drawing.Color.Gray
        Me.lblFormatGIF.Location = New System.Drawing.Point(213, 184)
        Me.lblFormatGIF.Name = "lblFormatGIF"
        Me.lblFormatGIF.Size = New System.Drawing.Size(54, 36)
        Me.lblFormatGIF.TabIndex = 5
        Me.lblFormatGIF.Text = "GIF"
        Me.lblFormatGIF.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ToolTipAffichage.SetToolTip(Me.lblFormatGIF, "Le format GIF est utilisé pour le Web, cependant sa qualité est assez mauvaise. ")
        '
        'lblFormatBMP
        '
        Me.lblFormatBMP.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormatBMP.ForeColor = System.Drawing.Color.Gray
        Me.lblFormatBMP.Location = New System.Drawing.Point(153, 184)
        Me.lblFormatBMP.Name = "lblFormatBMP"
        Me.lblFormatBMP.Size = New System.Drawing.Size(54, 36)
        Me.lblFormatBMP.TabIndex = 5
        Me.lblFormatBMP.Text = "BMP"
        Me.lblFormatBMP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ToolTipAffichage.SetToolTip(Me.lblFormatBMP, "Le format BMP présente une qualité parfaite puisqu'il n'est pas compressé ; ceci " & _
        "entraîne un poids très important.")
        '
        'lblFormatPNG
        '
        Me.lblFormatPNG.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormatPNG.ForeColor = System.Drawing.Color.Gray
        Me.lblFormatPNG.Location = New System.Drawing.Point(93, 184)
        Me.lblFormatPNG.Name = "lblFormatPNG"
        Me.lblFormatPNG.Size = New System.Drawing.Size(54, 36)
        Me.lblFormatPNG.TabIndex = 5
        Me.lblFormatPNG.Text = "PNG"
        Me.lblFormatPNG.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ToolTipAffichage.SetToolTip(Me.lblFormatPNG, "Le format PNG présente une très bonne qualité d'image et un poids modéré. Nous vo" & _
        "us recommandons de l'utiliser !")
        '
        'lblFormatJPG
        '
        Me.lblFormatJPG.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormatJPG.ForeColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.lblFormatJPG.Location = New System.Drawing.Point(33, 184)
        Me.lblFormatJPG.Name = "lblFormatJPG"
        Me.lblFormatJPG.Size = New System.Drawing.Size(54, 36)
        Me.lblFormatJPG.TabIndex = 5
        Me.lblFormatJPG.Text = "JPG"
        Me.lblFormatJPG.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ToolTipAffichage.SetToolTip(Me.lblFormatJPG, "Le format JPG présente l'intérêt d'être très léger. Cependant, sa forte compressi" & _
        "on entraîne souvent des captures d'écran de mauvaise qualité.")
        '
        'lblJokers3
        '
        Me.lblJokers3.AutoSize = True
        Me.lblJokers3.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJokers3.ForeColor = System.Drawing.Color.White
        Me.lblJokers3.Location = New System.Drawing.Point(226, 103)
        Me.lblJokers3.Name = "lblJokers3"
        Me.lblJokers3.Size = New System.Drawing.Size(96, 40)
        Me.lblJokers3.TabIndex = 2
        Me.lblJokers3.Text = "jour : \j ou \d" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "seconde : \s"
        '
        'lblJokers2
        '
        Me.lblJokers2.AutoSize = True
        Me.lblJokers2.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJokers2.ForeColor = System.Drawing.Color.White
        Me.lblJokers2.Location = New System.Drawing.Point(127, 103)
        Me.lblJokers2.Name = "lblJokers2"
        Me.lblJokers2.Size = New System.Drawing.Size(93, 40)
        Me.lblJokers2.TabIndex = 2
        Me.lblJokers2.Text = "mois : \m" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "minute : \mn" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'lblJokers
        '
        Me.lblJokers.AutoSize = True
        Me.lblJokers.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJokers.ForeColor = System.Drawing.Color.White
        Me.lblJokers.Location = New System.Drawing.Point(9, 103)
        Me.lblJokers.Name = "lblJokers"
        Me.lblJokers.Size = New System.Drawing.Size(112, 40)
        Me.lblJokers.TabIndex = 2
        Me.lblJokers.Text = "année : \y ou \a" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "heure : \s"
        '
        'lblResultat
        '
        Me.lblResultat.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResultat.ForeColor = System.Drawing.Color.White
        Me.lblResultat.Location = New System.Drawing.Point(13, 62)
        Me.lblResultat.Name = "lblResultat"
        Me.lblResultat.Size = New System.Drawing.Size(470, 20)
        Me.lblResultat.TabIndex = 2
        Me.lblResultat.Text = "entrez un nom"
        Me.lblResultat.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.ToolTipAffichage.SetToolTip(Me.lblResultat, "Si vous preniez une capture d'écran maintenant, son nom serait celui-là.")
        '
        'lblAutresParametres
        '
        Me.lblAutresParametres.AutoSize = True
        Me.lblAutresParametres.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAutresParametres.ForeColor = System.Drawing.Color.White
        Me.lblAutresParametres.Location = New System.Drawing.Point(9, 9)
        Me.lblAutresParametres.Name = "lblAutresParametres"
        Me.lblAutresParametres.Size = New System.Drawing.Size(105, 20)
        Me.lblAutresParametres.TabIndex = 2
        Me.lblAutresParametres.Text = "nom du fichier"
        '
        'lblSelectionFormat
        '
        Me.lblSelectionFormat.AutoSize = True
        Me.lblSelectionFormat.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectionFormat.ForeColor = System.Drawing.Color.White
        Me.lblSelectionFormat.Location = New System.Drawing.Point(9, 160)
        Me.lblSelectionFormat.Name = "lblSelectionFormat"
        Me.lblSelectionFormat.Size = New System.Drawing.Size(170, 20)
        Me.lblSelectionFormat.TabIndex = 2
        Me.lblSelectionFormat.Text = "format d'enregistrement"
        Me.ToolTipAffichage.SetToolTip(Me.lblSelectionFormat, "Sélectionnez ici le format d'enregistrement pour vos captures d'écran.")
        '
        'ongletRaccourcis
        '
        Me.ongletRaccourcis.AutoSize = True
        Me.ongletRaccourcis.Font = New System.Drawing.Font("Segoe WP", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ongletRaccourcis.ForeColor = System.Drawing.Color.Gray
        Me.ongletRaccourcis.Location = New System.Drawing.Point(241, 9)
        Me.ongletRaccourcis.Name = "ongletRaccourcis"
        Me.ongletRaccourcis.Size = New System.Drawing.Size(120, 32)
        Me.ongletRaccourcis.TabIndex = 2
        Me.ongletRaccourcis.Text = "raccourcis"
        '
        'ongletPartage
        '
        Me.ongletPartage.AutoSize = True
        Me.ongletPartage.Font = New System.Drawing.Font("Segoe WP", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ongletPartage.ForeColor = System.Drawing.Color.Gray
        Me.ongletPartage.Location = New System.Drawing.Point(367, 9)
        Me.ongletPartage.Name = "ongletPartage"
        Me.ongletPartage.Size = New System.Drawing.Size(97, 32)
        Me.ongletPartage.TabIndex = 2
        Me.ongletPartage.Text = "partage"
        '
        'panelRaccourcis
        '
        Me.panelRaccourcis.Controls.Add(Me.cbHQ)
        Me.panelRaccourcis.Controls.Add(Me.lblRaccourciImgur)
        Me.panelRaccourcis.Controls.Add(Me.lblraccourciEdit)
        Me.panelRaccourcis.Controls.Add(Me.raccourciImgur)
        Me.panelRaccourcis.Controls.Add(Me.lblraccourciTwitter)
        Me.panelRaccourcis.Controls.Add(Me.raccourciEdit)
        Me.panelRaccourcis.Controls.Add(Me.raccourciTwitter)
        Me.panelRaccourcis.Controls.Add(Me.lblStopCapture)
        Me.panelRaccourcis.Controls.Add(Me.tbContenuTweet)
        Me.panelRaccourcis.Controls.Add(Me.raccourciScreenshot)
        Me.panelRaccourcis.Controls.Add(Me.lblRaccourciScreenshot)
        Me.panelRaccourcis.Location = New System.Drawing.Point(18, 315)
        Me.panelRaccourcis.Name = "panelRaccourcis"
        Me.panelRaccourcis.Size = New System.Drawing.Size(495, 260)
        Me.panelRaccourcis.TabIndex = 5
        '
        'cbHQ
        '
        Me.cbHQ.AutoSize = True
        Me.cbHQ.Font = New System.Drawing.Font("Segoe WP", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbHQ.ForeColor = System.Drawing.Color.White
        Me.cbHQ.Location = New System.Drawing.Point(12, 149)
        Me.cbHQ.Name = "cbHQ"
        Me.cbHQ.Size = New System.Drawing.Size(357, 24)
        Me.cbHQ.TabIndex = 3
        Me.cbHQ.Text = "toujours sauvegarder une version haute résolution"
        Me.cbHQ.UseVisualStyleBackColor = True
        Me.cbHQ.Visible = False
        '
        'lblRaccourciImgur
        '
        Me.lblRaccourciImgur.AutoSize = True
        Me.lblRaccourciImgur.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRaccourciImgur.ForeColor = System.Drawing.Color.White
        Me.lblRaccourciImgur.Location = New System.Drawing.Point(8, 82)
        Me.lblRaccourciImgur.Name = "lblRaccourciImgur"
        Me.lblRaccourciImgur.Size = New System.Drawing.Size(168, 20)
        Me.lblRaccourciImgur.TabIndex = 2
        Me.lblRaccourciImgur.Text = "capture d'écran + Imgur"
        Me.ToolTipAffichage.SetToolTip(Me.lblRaccourciImgur, "Enregistrer une capture d'écran puis la partager directement sur Imgur.")
        '
        'lblraccourciEdit
        '
        Me.lblraccourciEdit.AutoSize = True
        Me.lblraccourciEdit.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblraccourciEdit.ForeColor = System.Drawing.Color.White
        Me.lblraccourciEdit.Location = New System.Drawing.Point(8, 33)
        Me.lblraccourciEdit.Name = "lblraccourciEdit"
        Me.lblraccourciEdit.Size = New System.Drawing.Size(176, 20)
        Me.lblraccourciEdit.TabIndex = 2
        Me.lblraccourciEdit.Text = "capture d'écran + Edition"
        Me.ToolTipAffichage.SetToolTip(Me.lblraccourciEdit, "Enregistrer une capture d'écran puis afficher la fenêtre d'édition.")
        '
        'raccourciImgur
        '
        Me.raccourciImgur.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.raccourciImgur.ForeColor = System.Drawing.Color.White
        Me.raccourciImgur.Location = New System.Drawing.Point(200, 82)
        Me.raccourciImgur.Name = "raccourciImgur"
        Me.raccourciImgur.Size = New System.Drawing.Size(282, 20)
        Me.raccourciImgur.TabIndex = 2
        Me.raccourciImgur.Text = "non défini"
        Me.raccourciImgur.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblraccourciTwitter
        '
        Me.lblraccourciTwitter.AutoSize = True
        Me.lblraccourciTwitter.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblraccourciTwitter.ForeColor = System.Drawing.Color.White
        Me.lblraccourciTwitter.Location = New System.Drawing.Point(7, 57)
        Me.lblraccourciTwitter.Name = "lblraccourciTwitter"
        Me.lblraccourciTwitter.Size = New System.Drawing.Size(174, 20)
        Me.lblraccourciTwitter.TabIndex = 2
        Me.lblraccourciTwitter.Text = "capture d'écran + Twitter"
        Me.ToolTipAffichage.SetToolTip(Me.lblraccourciTwitter, "Enregistrer une capture d'écran puis la partager directement sur Twitter.")
        '
        'raccourciEdit
        '
        Me.raccourciEdit.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.raccourciEdit.ForeColor = System.Drawing.Color.White
        Me.raccourciEdit.Location = New System.Drawing.Point(200, 33)
        Me.raccourciEdit.Name = "raccourciEdit"
        Me.raccourciEdit.Size = New System.Drawing.Size(282, 20)
        Me.raccourciEdit.TabIndex = 2
        Me.raccourciEdit.Text = "non défini"
        Me.raccourciEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'raccourciTwitter
        '
        Me.raccourciTwitter.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.raccourciTwitter.ForeColor = System.Drawing.Color.White
        Me.raccourciTwitter.Location = New System.Drawing.Point(199, 57)
        Me.raccourciTwitter.Name = "raccourciTwitter"
        Me.raccourciTwitter.Size = New System.Drawing.Size(282, 20)
        Me.raccourciTwitter.TabIndex = 2
        Me.raccourciTwitter.Text = "non défini"
        Me.raccourciTwitter.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblStopCapture
        '
        Me.lblStopCapture.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStopCapture.ForeColor = System.Drawing.Color.White
        Me.lblStopCapture.Location = New System.Drawing.Point(8, 117)
        Me.lblStopCapture.Name = "lblStopCapture"
        Me.lblStopCapture.Size = New System.Drawing.Size(474, 20)
        Me.lblStopCapture.TabIndex = 2
        Me.lblStopCapture.Text = "cliquez ici pour arrêter la capture"
        Me.lblStopCapture.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblStopCapture.Visible = False
        '
        'tbContenuTweet
        '
        Me.tbContenuTweet.Font = New System.Drawing.Font("Segoe WP", 11.25!)
        Me.tbContenuTweet.Location = New System.Drawing.Point(12, 187)
        Me.tbContenuTweet.Name = "tbContenuTweet"
        Me.tbContenuTweet.Size = New System.Drawing.Size(453, 27)
        Me.tbContenuTweet.TabIndex = 9
        Me.tbContenuTweet.Text = "Bonjour, ceci est un tweet de test !"
        Me.ToolTipAffichage.SetToolTip(Me.tbContenuTweet, "Entrez ici un texte pour votre tweet.")
        Me.tbContenuTweet.Visible = False
        '
        'raccourciScreenshot
        '
        Me.raccourciScreenshot.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.raccourciScreenshot.ForeColor = System.Drawing.Color.White
        Me.raccourciScreenshot.Location = New System.Drawing.Point(200, 9)
        Me.raccourciScreenshot.Name = "raccourciScreenshot"
        Me.raccourciScreenshot.Size = New System.Drawing.Size(282, 20)
        Me.raccourciScreenshot.TabIndex = 2
        Me.raccourciScreenshot.Text = "non défini"
        Me.raccourciScreenshot.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblRaccourciScreenshot
        '
        Me.lblRaccourciScreenshot.AutoSize = True
        Me.lblRaccourciScreenshot.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRaccourciScreenshot.ForeColor = System.Drawing.Color.White
        Me.lblRaccourciScreenshot.Location = New System.Drawing.Point(8, 9)
        Me.lblRaccourciScreenshot.Name = "lblRaccourciScreenshot"
        Me.lblRaccourciScreenshot.Size = New System.Drawing.Size(111, 20)
        Me.lblRaccourciScreenshot.TabIndex = 2
        Me.lblRaccourciScreenshot.Text = "capture d'écran"
        Me.ToolTipAffichage.SetToolTip(Me.lblRaccourciScreenshot, "Enregistrer une capture d'écran sans l'éditer.")
        '
        'panelPartage
        '
        Me.panelPartage.Controls.Add(Me.lblConsumerSecret)
        Me.panelPartage.Controls.Add(Me.tbConsumerSecret)
        Me.panelPartage.Controls.Add(Me.lblTitleCS)
        Me.panelPartage.Controls.Add(Me.lblConsumerKey)
        Me.panelPartage.Controls.Add(Me.tbConsumerKey)
        Me.panelPartage.Controls.Add(Me.lblTitleCK)
        Me.panelPartage.Controls.Add(Me.lblAccessTokenSecret)
        Me.panelPartage.Controls.Add(Me.lblInfosTwitter)
        Me.panelPartage.Controls.Add(Me.bValiderConsumerSecret)
        Me.panelPartage.Controls.Add(Me.bValiderAccessTokenSecret)
        Me.panelPartage.Controls.Add(Me.lblSendTweet)
        Me.panelPartage.Controls.Add(Me.lblTitleAT)
        Me.panelPartage.Controls.Add(Me.bValiderConsumerKey)
        Me.panelPartage.Controls.Add(Me.bValiderAccessToken)
        Me.panelPartage.Controls.Add(Me.lblAccessToken)
        Me.panelPartage.Controls.Add(Me.tbAccessToken)
        Me.panelPartage.Controls.Add(Me.tbAccessTokenSecret)
        Me.panelPartage.Controls.Add(Me.lblTitleATS)
        Me.panelPartage.Location = New System.Drawing.Point(557, 315)
        Me.panelPartage.Name = "panelPartage"
        Me.panelPartage.Size = New System.Drawing.Size(495, 260)
        Me.panelPartage.TabIndex = 5
        '
        'lblConsumerSecret
        '
        Me.lblConsumerSecret.Font = New System.Drawing.Font("Consolas", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblConsumerSecret.ForeColor = System.Drawing.Color.White
        Me.lblConsumerSecret.Location = New System.Drawing.Point(30, 118)
        Me.lblConsumerSecret.Name = "lblConsumerSecret"
        Me.lblConsumerSecret.Size = New System.Drawing.Size(453, 20)
        Me.lblConsumerSecret.TabIndex = 19
        Me.lblConsumerSecret.Text = "[ConsumerSecret]"
        '
        'tbConsumerSecret
        '
        Me.tbConsumerSecret.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbConsumerSecret.Location = New System.Drawing.Point(30, 115)
        Me.tbConsumerSecret.Name = "tbConsumerSecret"
        Me.tbConsumerSecret.Size = New System.Drawing.Size(453, 26)
        Me.tbConsumerSecret.TabIndex = 18
        Me.tbConsumerSecret.Visible = False
        '
        'lblTitleCS
        '
        Me.lblTitleCS.AutoSize = True
        Me.lblTitleCS.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitleCS.ForeColor = System.Drawing.Color.White
        Me.lblTitleCS.Location = New System.Drawing.Point(29, 91)
        Me.lblTitleCS.Name = "lblTitleCS"
        Me.lblTitleCS.Size = New System.Drawing.Size(81, 20)
        Me.lblTitleCS.TabIndex = 17
        Me.lblTitleCS.Text = "Clé secrète"
        '
        'lblConsumerKey
        '
        Me.lblConsumerKey.Font = New System.Drawing.Font("Consolas", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblConsumerKey.ForeColor = System.Drawing.Color.White
        Me.lblConsumerKey.Location = New System.Drawing.Point(30, 63)
        Me.lblConsumerKey.Name = "lblConsumerKey"
        Me.lblConsumerKey.Size = New System.Drawing.Size(453, 20)
        Me.lblConsumerKey.TabIndex = 16
        Me.lblConsumerKey.Text = "[ConsumerKey]"
        '
        'tbConsumerKey
        '
        Me.tbConsumerKey.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbConsumerKey.Location = New System.Drawing.Point(30, 60)
        Me.tbConsumerKey.Name = "tbConsumerKey"
        Me.tbConsumerKey.Size = New System.Drawing.Size(453, 26)
        Me.tbConsumerKey.TabIndex = 15
        Me.tbConsumerKey.Visible = False
        '
        'lblTitleCK
        '
        Me.lblTitleCK.AutoSize = True
        Me.lblTitleCK.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitleCK.ForeColor = System.Drawing.Color.White
        Me.lblTitleCK.Location = New System.Drawing.Point(29, 36)
        Me.lblTitleCK.Name = "lblTitleCK"
        Me.lblTitleCK.Size = New System.Drawing.Size(121, 20)
        Me.lblTitleCK.TabIndex = 14
        Me.lblTitleCK.Text = "Clé d'application"
        '
        'lblAccessTokenSecret
        '
        Me.lblAccessTokenSecret.Font = New System.Drawing.Font("Consolas", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccessTokenSecret.ForeColor = System.Drawing.Color.White
        Me.lblAccessTokenSecret.Location = New System.Drawing.Point(30, 226)
        Me.lblAccessTokenSecret.Name = "lblAccessTokenSecret"
        Me.lblAccessTokenSecret.Size = New System.Drawing.Size(453, 20)
        Me.lblAccessTokenSecret.TabIndex = 13
        Me.lblAccessTokenSecret.Text = "[AccessTokenSecret]"
        '
        'lblInfosTwitter
        '
        Me.lblInfosTwitter.AutoSize = True
        Me.lblInfosTwitter.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInfosTwitter.ForeColor = System.Drawing.Color.White
        Me.lblInfosTwitter.Location = New System.Drawing.Point(9, 9)
        Me.lblInfosTwitter.Name = "lblInfosTwitter"
        Me.lblInfosTwitter.Size = New System.Drawing.Size(235, 20)
        Me.lblInfosTwitter.TabIndex = 2
        Me.lblInfosTwitter.Text = "informations de connexion Twitter"
        '
        'bValiderConsumerSecret
        '
        Me.bValiderConsumerSecret.Location = New System.Drawing.Point(5, 116)
        Me.bValiderConsumerSecret.Name = "bValiderConsumerSecret"
        Me.bValiderConsumerSecret.Size = New System.Drawing.Size(22, 23)
        Me.bValiderConsumerSecret.TabIndex = 7
        Me.bValiderConsumerSecret.Text = "Button1"
        Me.bValiderConsumerSecret.UseVisualStyleBackColor = True
        '
        'bValiderAccessTokenSecret
        '
        Me.bValiderAccessTokenSecret.Location = New System.Drawing.Point(5, 225)
        Me.bValiderAccessTokenSecret.Name = "bValiderAccessTokenSecret"
        Me.bValiderAccessTokenSecret.Size = New System.Drawing.Size(22, 23)
        Me.bValiderAccessTokenSecret.TabIndex = 7
        Me.bValiderAccessTokenSecret.Text = "Button1"
        Me.bValiderAccessTokenSecret.UseVisualStyleBackColor = True
        '
        'lblSendTweet
        '
        Me.lblSendTweet.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSendTweet.ForeColor = System.Drawing.Color.White
        Me.lblSendTweet.Location = New System.Drawing.Point(201, 119)
        Me.lblSendTweet.Name = "lblSendTweet"
        Me.lblSendTweet.Size = New System.Drawing.Size(282, 20)
        Me.lblSendTweet.TabIndex = 2
        Me.lblSendTweet.Text = "envoyer le tweet"
        Me.lblSendTweet.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblSendTweet.Visible = False
        '
        'lblTitleAT
        '
        Me.lblTitleAT.AutoSize = True
        Me.lblTitleAT.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitleAT.ForeColor = System.Drawing.Color.White
        Me.lblTitleAT.Location = New System.Drawing.Point(29, 146)
        Me.lblTitleAT.Name = "lblTitleAT"
        Me.lblTitleAT.Size = New System.Drawing.Size(96, 20)
        Me.lblTitleAT.TabIndex = 2
        Me.lblTitleAT.Text = "Jeton d'accès"
        '
        'bValiderConsumerKey
        '
        Me.bValiderConsumerKey.Location = New System.Drawing.Point(5, 63)
        Me.bValiderConsumerKey.Name = "bValiderConsumerKey"
        Me.bValiderConsumerKey.Size = New System.Drawing.Size(22, 23)
        Me.bValiderConsumerKey.TabIndex = 7
        Me.bValiderConsumerKey.Text = "Button1"
        Me.bValiderConsumerKey.UseVisualStyleBackColor = True
        '
        'bValiderAccessToken
        '
        Me.bValiderAccessToken.Location = New System.Drawing.Point(4, 171)
        Me.bValiderAccessToken.Name = "bValiderAccessToken"
        Me.bValiderAccessToken.Size = New System.Drawing.Size(22, 23)
        Me.bValiderAccessToken.TabIndex = 7
        Me.bValiderAccessToken.Text = "Button1"
        Me.bValiderAccessToken.UseVisualStyleBackColor = True
        '
        'lblAccessToken
        '
        Me.lblAccessToken.Font = New System.Drawing.Font("Consolas", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccessToken.ForeColor = System.Drawing.Color.White
        Me.lblAccessToken.Location = New System.Drawing.Point(30, 173)
        Me.lblAccessToken.Name = "lblAccessToken"
        Me.lblAccessToken.Size = New System.Drawing.Size(453, 20)
        Me.lblAccessToken.TabIndex = 10
        Me.lblAccessToken.Text = "[AccessToken]"
        '
        'tbAccessToken
        '
        Me.tbAccessToken.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbAccessToken.Location = New System.Drawing.Point(30, 170)
        Me.tbAccessToken.Name = "tbAccessToken"
        Me.tbAccessToken.Size = New System.Drawing.Size(453, 26)
        Me.tbAccessToken.TabIndex = 9
        '
        'tbAccessTokenSecret
        '
        Me.tbAccessTokenSecret.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbAccessTokenSecret.Location = New System.Drawing.Point(30, 223)
        Me.tbAccessTokenSecret.Name = "tbAccessTokenSecret"
        Me.tbAccessTokenSecret.Size = New System.Drawing.Size(453, 26)
        Me.tbAccessTokenSecret.TabIndex = 12
        '
        'lblTitleATS
        '
        Me.lblTitleATS.AutoSize = True
        Me.lblTitleATS.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitleATS.ForeColor = System.Drawing.Color.White
        Me.lblTitleATS.Location = New System.Drawing.Point(29, 199)
        Me.lblTitleATS.Name = "lblTitleATS"
        Me.lblTitleATS.Size = New System.Drawing.Size(139, 20)
        Me.lblTitleATS.TabIndex = 11
        Me.lblTitleATS.Text = "Jeton d'accès secret"
        '
        'TimerHotKeys
        '
        Me.TimerHotKeys.Enabled = True
        '
        'TimerFilename
        '
        Me.TimerFilename.Interval = 1000
        '
        'IconeNotification
        '
        Me.IconeNotification.ContextMenuStrip = Me.MenuNotification
        Me.IconeNotification.Icon = CType(resources.GetObject("IconeNotification.Icon"), System.Drawing.Icon)
        Me.IconeNotification.Text = "ScreenIt!"
        Me.IconeNotification.Visible = True
        '
        'MenuNotification
        '
        Me.MenuNotification.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LibrairieToolStripMenuItem, Me.RéglagesToolStripMenuItem, Me.NotificationsToolStripMenuItem, Me.QuitterToolStripMenuItem})
        Me.MenuNotification.Name = "MenuNotification"
        Me.MenuNotification.Size = New System.Drawing.Size(143, 92)
        '
        'LibrairieToolStripMenuItem
        '
        Me.LibrairieToolStripMenuItem.Name = "LibrairieToolStripMenuItem"
        Me.LibrairieToolStripMenuItem.Size = New System.Drawing.Size(142, 22)
        Me.LibrairieToolStripMenuItem.Text = "Librairie"
        '
        'RéglagesToolStripMenuItem
        '
        Me.RéglagesToolStripMenuItem.Name = "RéglagesToolStripMenuItem"
        Me.RéglagesToolStripMenuItem.Size = New System.Drawing.Size(142, 22)
        Me.RéglagesToolStripMenuItem.Text = "Réglages"
        '
        'NotificationsToolStripMenuItem
        '
        Me.NotificationsToolStripMenuItem.Name = "NotificationsToolStripMenuItem"
        Me.NotificationsToolStripMenuItem.Size = New System.Drawing.Size(142, 22)
        Me.NotificationsToolStripMenuItem.Text = "Notifications"
        '
        'QuitterToolStripMenuItem
        '
        Me.QuitterToolStripMenuItem.Name = "QuitterToolStripMenuItem"
        Me.QuitterToolStripMenuItem.Size = New System.Drawing.Size(142, 22)
        Me.QuitterToolStripMenuItem.Text = "Quitter"
        '
        'lblFermer
        '
        Me.lblFermer.AutoSize = True
        Me.lblFermer.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFermer.ForeColor = System.Drawing.Color.White
        Me.lblFermer.Location = New System.Drawing.Point(522, 1)
        Me.lblFermer.Name = "lblFermer"
        Me.lblFermer.Size = New System.Drawing.Size(16, 20)
        Me.lblFermer.TabIndex = 9
        Me.lblFermer.Text = "x"
        Me.ToolTipAffichage.SetToolTip(Me.lblFermer, "Sélectionnez ici le format d'enregistrement pour vos captures d'écran.")
        '
        'Principal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1192, 602)
        Me.Controls.Add(Me.lblFermer)
        Me.Controls.Add(Me.panelPartage)
        Me.Controls.Add(Me.panelRaccourcis)
        Me.Controls.Add(Me.panelQualite)
        Me.Controls.Add(Me.panelRepertoires)
        Me.Controls.Add(Me.ongletPartage)
        Me.Controls.Add(Me.ongletRaccourcis)
        Me.Controls.Add(Me.ongletQualite)
        Me.Controls.Add(Me.ongletRepertoires)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.Name = "Principal"
        Me.Opacity = 0.0R
        Me.ShowInTaskbar = False
        Me.Text = "ScreenIt - Paramètres"
        Me.panelRepertoires.ResumeLayout(False)
        Me.panelRepertoires.PerformLayout()
        Me.panelQualite.ResumeLayout(False)
        Me.panelQualite.PerformLayout()
        Me.panelRaccourcis.ResumeLayout(False)
        Me.panelRaccourcis.PerformLayout()
        Me.panelPartage.ResumeLayout(False)
        Me.panelPartage.PerformLayout()
        Me.MenuNotification.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents panelRepertoires As System.Windows.Forms.Panel
    Friend WithEvents lblRepHQ As System.Windows.Forms.Label
    Friend WithEvents ongletRepertoires As System.Windows.Forms.Label
    Friend WithEvents lblLocationTemp As System.Windows.Forms.Label
    Friend WithEvents lblLocationHQ As System.Windows.Forms.Label
    Friend WithEvents lblRepTemp As System.Windows.Forms.Label
    Friend WithEvents ongletQualite As System.Windows.Forms.Label
    Friend WithEvents panelQualite As System.Windows.Forms.Panel
    Friend WithEvents lblSelectionFormat As System.Windows.Forms.Label
    Friend WithEvents ongletRaccourcis As System.Windows.Forms.Label
    Friend WithEvents ongletPartage As System.Windows.Forms.Label
    Friend WithEvents lblFormatTIFF As System.Windows.Forms.Label
    Friend WithEvents lblFormatGIF As System.Windows.Forms.Label
    Friend WithEvents lblFormatBMP As System.Windows.Forms.Label
    Friend WithEvents lblFormatPNG As System.Windows.Forms.Label
    Friend WithEvents lblFormatJPG As System.Windows.Forms.Label
    Friend WithEvents lblAutresParametres As System.Windows.Forms.Label
    Friend WithEvents panelRaccourcis As System.Windows.Forms.Panel
    Friend WithEvents lblRaccourciScreenshot As System.Windows.Forms.Label
    Friend WithEvents panelPartage As System.Windows.Forms.Panel
    Friend WithEvents lblInfosTwitter As System.Windows.Forms.Label
    Friend WithEvents lblraccourciEdit As System.Windows.Forms.Label
    Friend WithEvents lblraccourciTwitter As System.Windows.Forms.Label
    Friend WithEvents raccourciEdit As System.Windows.Forms.Label
    Friend WithEvents raccourciTwitter As System.Windows.Forms.Label
    Friend WithEvents raccourciScreenshot As System.Windows.Forms.Label
    Friend WithEvents TimerHotKeys As System.Windows.Forms.Timer
    Friend WithEvents lblStopCapture As System.Windows.Forms.Label
    Friend WithEvents tbNomFichier As System.Windows.Forms.TextBox
    Friend WithEvents lblJokers2 As System.Windows.Forms.Label
    Friend WithEvents lblJokers As System.Windows.Forms.Label
    Friend WithEvents lblResultat As System.Windows.Forms.Label
    Friend WithEvents TimerFilename As System.Windows.Forms.Timer
    Friend WithEvents tbLocationHQ As System.Windows.Forms.TextBox
    Friend WithEvents bValiderAdresseHQ As System.Windows.Forms.Button
    Friend WithEvents bValiderTemp As System.Windows.Forms.Button
    Friend WithEvents tbLocationTemp As System.Windows.Forms.TextBox
    Friend WithEvents lblJokers3 As System.Windows.Forms.Label
    Friend WithEvents cbHQ As System.Windows.Forms.CheckBox
    Friend WithEvents lblFilenameFormat As System.Windows.Forms.Label
    Friend WithEvents bValiderFilenameFormat As System.Windows.Forms.Button
    Friend WithEvents lblAccessTokenSecret As System.Windows.Forms.Label
    Friend WithEvents tbAccessTokenSecret As System.Windows.Forms.TextBox
    Friend WithEvents lblTitleATS As System.Windows.Forms.Label
    Friend WithEvents lblAccessToken As System.Windows.Forms.Label
    Friend WithEvents tbAccessToken As System.Windows.Forms.TextBox
    Friend WithEvents lblTitleAT As System.Windows.Forms.Label
    Friend WithEvents tbContenuTweet As System.Windows.Forms.TextBox
    Friend WithEvents lblSendTweet As System.Windows.Forms.Label
    Friend WithEvents bValiderAccessTokenSecret As System.Windows.Forms.Button
    Friend WithEvents bValiderAccessToken As System.Windows.Forms.Button
    Friend WithEvents IconeNotification As System.Windows.Forms.NotifyIcon
    Friend WithEvents ToolTipAffichage As System.Windows.Forms.ToolTip
    Friend WithEvents lblFermer As System.Windows.Forms.Label
    Friend WithEvents lblFormatBPG As System.Windows.Forms.Label
    Friend WithEvents lblConsumerSecret As System.Windows.Forms.Label
    Friend WithEvents tbConsumerSecret As System.Windows.Forms.TextBox
    Friend WithEvents lblTitleCS As System.Windows.Forms.Label
    Friend WithEvents lblConsumerKey As System.Windows.Forms.Label
    Friend WithEvents tbConsumerKey As System.Windows.Forms.TextBox
    Friend WithEvents lblTitleCK As System.Windows.Forms.Label
    Friend WithEvents bValiderConsumerSecret As System.Windows.Forms.Button
    Friend WithEvents bValiderConsumerKey As System.Windows.Forms.Button
    Friend WithEvents MenuNotification As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents QuitterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LibrairieToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RéglagesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NotificationsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblRaccourciImgur As System.Windows.Forms.Label
    Friend WithEvents raccourciImgur As System.Windows.Forms.Label

End Class
