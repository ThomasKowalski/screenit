﻿Module PublicStuff
    Public KeysScreenshot As Keys() = {Nothing, Nothing, Nothing, Nothing, Nothing}
    Public KeysScreenShotEdit As Keys() = {Nothing, Nothing, Nothing, Nothing, Nothing}
    Public KeysScreenshotTwitter As Keys() = {Nothing, Nothing, Nothing, Nothing, Nothing}
    Public KeysScreenshotImgur As Keys() = {Nothing, Nothing, Nothing, Nothing, Nothing}
    Public RepertoireSauvegarde As String = ""
    Public RepertoireTemporaire As String = ""
    Public FormatNomScreenshot As String = ""
    Public FormatSauvegarde As String = ""
    Public FormatSauvegardeImageFormat As Imaging.ImageFormat = Nothing
    Public TempString As String = ""
    Public ConserveHD As Boolean = False
    Public CleAPI As String = ""
    Public CleAPISecret As String = ""
    Public ConsumerKey As String = ""
    Public ConsumerSecret As String = ""
    Public AccessToken As String = ""
    Public AccessTokenSecret As String = ""
    Public PublicCounter As Integer = 0
    Public ShowNotifications As Boolean = True

    Public Sub EcrireRegistre(Nom As String, Valeur As String)
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\ScreenIt\", Nom, Valeur)
    End Sub

    Public Function LireRegistre(Nom As String) As String
        Return My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\ScreenIt\", Nom, "")
    End Function

    Public Sub LoadOptions()
        RepertoireSauvegarde = LireRegistre("SaveLocation")
        If RepertoireSauvegarde = "" Then
            EcrireRegistre("SaveLocation", Environ("USERPROFILE") & "\Images\Captures d'écran")
            My.Computer.FileSystem.CreateDirectory(Environ("USERPROFILE") & "\Images\Captures d'écran")
            LoadOptions()
            Exit Sub
        End If
        RepertoireTemporaire = LireRegistre("TempLocation")
        If RepertoireTemporaire = "" Then
            EcrireRegistre("TempLocation", Environ("TEMP"))
            LoadOptions()
            Exit Sub
        End If
        RepertoireTemporaire &= "\ScreenIt\"
        If My.Computer.FileSystem.DirectoryExists(RepertoireTemporaire) = False Then : My.Computer.FileSystem.CreateDirectory(RepertoireTemporaire)
        End If
        FormatNomScreenshot = LireRegistre("FilenameFormat")
        If FormatNomScreenshot = "" Then
            EcrireRegistre("FilenameFormat", Environ("Capture d'écran du \y-\m-\d à \hh \mnmn et \ss"))
            LoadOptions()
            Exit Sub
        End If
        AccessToken = LireRegistre("TwitterAccessToken")
        AccessTokenSecret = LireRegistre("TwitterAccessTokenSecret")
        ConsumerKey = LireRegistre("TwitterConsumerKey")
        ConsumerSecret = LireRegistre("TwitterConsumerSecret")
        Dim strTemp As String = LireRegistre("ConserveHD")
        If strTemp = "" Then
            EcrireRegistre("ConserveHD", "True")
            LoadOptions()
            Exit Sub
        Else
            ConserveHD = LireRegistre("ConserveHD")
        End If
        FormatSauvegarde = LireRegistre("Format")
        If FormatSauvegarde = "" Then
            EcrireRegistre("Format", "BMP")
            LoadOptions()
            Exit Sub
        End If
        ChargerFormatEnregistrement()

        Dim TempKeysScreenshot = LireRegistre("Raccourci")
        If TempKeysScreenshot <> "" Then
            For i = 0 To 4
                KeysScreenshot(i) = CType(TempKeysScreenshot.Split(";")(i), Keys)
            Next
        End If
        TempKeysScreenshot = LireRegistre("RaccourciTwitter")
        If TempKeysScreenshot <> "" Then
            For i = 0 To 4
                KeysScreenshotTwitter(i) = CType(TempKeysScreenshot.Split(";")(i), Keys)
            Next
        End If
        TempKeysScreenshot = LireRegistre("RaccourciPaint")
        If TempKeysScreenshot <> "" Then
            For i = 0 To 4
                KeysScreenShotEdit(i) = CType(TempKeysScreenshot.Split(";")(i), Keys)
            Next
        End If
        TempKeysScreenshot = LireRegistre("RaccourciImgur")
        If TempKeysScreenshot <> "" Then
            For i = 0 To 4
                KeysScreenshotImgur(i) = CType(TempKeysScreenshot.Split(";")(i), Keys)
            Next
        End If
        If LireRegistre("Notifications") = "" Then
            EcrireRegistre("Notifications", "True")
        End If
        Try
            ShowNotifications = CBool(LireRegistre("Notifications"))
        Catch ex As Exception
            EcrireRegistre("Notifications", "True")
            LoadOptions()
        End Try
    End Sub

    Public Function TakeScreenshot() As Image
        Dim bounds As Rectangle
        Dim screenshot As System.Drawing.Bitmap
        Dim graph As Graphics
        bounds = Screen.PrimaryScreen.Bounds
        screenshot = New System.Drawing.Bitmap(bounds.Width, bounds.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
        graph = Graphics.FromImage(screenshot)
        graph.CopyFromScreen(bounds.X, bounds.Y, 0, 0, bounds.Size, CopyPixelOperation.SourceCopy)
        Return screenshot
    End Function

    Private Sub ChargerFormatEnregistrement()
        Dim TempFormat As Imaging.ImageFormat = Imaging.ImageFormat.Bmp
        Select Case FormatSauvegarde
            Case "JPG"
                TempFormat = Imaging.ImageFormat.Jpeg
            Case "GIF"
                TempFormat = Imaging.ImageFormat.Gif
            Case "PNG"
                TempFormat = Imaging.ImageFormat.Png
            Case "BMP"
                TempFormat = Imaging.ImageFormat.Bmp
            Case "TIFF"
                TempFormat = Imaging.ImageFormat.Tiff
        End Select
        FormatSauvegardeImageFormat = TempFormat
    End Sub

    Sub CleanUpPlease()
        Dim ListeChemins As New List(Of String)
        ListeChemins.Add(RepertoireTemporaire & "temp." & FormatSauvegarde)
        For Each item In ListeChemins
            Try
                My.Computer.FileSystem.DeleteFile(item)
            Catch ex As Exception

            End Try
        Next
        Try
            My.Computer.FileSystem.DeleteDirectory(RepertoireTemporaire, FileIO.DeleteDirectoryOption.DeleteAllContents)
            My.Computer.FileSystem.CreateDirectory(RepertoireTemporaire)
        Catch ex As Exception

        End Try
    End Sub
End Module
