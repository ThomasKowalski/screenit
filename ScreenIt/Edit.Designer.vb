﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Edit
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MainPictureBox = New System.Windows.Forms.PictureBox()
        Me.PanelPictureBox = New System.Windows.Forms.Panel()
        Me.lblTitreFenetre = New System.Windows.Forms.Label()
        Me.lblRecommencer = New System.Windows.Forms.Label()
        Me.lblSauvegarder = New System.Windows.Forms.Label()
        Me.lblPartagerTwitter = New System.Windows.Forms.Label()
        Me.lblSupprimer = New System.Windows.Forms.Label()
        Me.PanelBoutons = New System.Windows.Forms.Panel()
        Me.bValiderNomCapture = New System.Windows.Forms.Button()
        Me.lblNomCapture = New System.Windows.Forms.Label()
        Me.tbNomCapture = New System.Windows.Forms.TextBox()
        Me.lblPartagerImgur = New System.Windows.Forms.Label()
        Me.tbUrlImgur = New System.Windows.Forms.TextBox()
        Me.bwImgur = New System.ComponentModel.BackgroundWorker()
        CType(Me.MainPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelPictureBox.SuspendLayout()
        Me.PanelBoutons.SuspendLayout()
        Me.SuspendLayout()
        '
        'MainPictureBox
        '
        Me.MainPictureBox.Location = New System.Drawing.Point(0, 0)
        Me.MainPictureBox.Name = "MainPictureBox"
        Me.MainPictureBox.Size = New System.Drawing.Size(200, 300)
        Me.MainPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.MainPictureBox.TabIndex = 0
        Me.MainPictureBox.TabStop = False
        '
        'PanelPictureBox
        '
        Me.PanelPictureBox.BackColor = System.Drawing.Color.Black
        Me.PanelPictureBox.Controls.Add(Me.MainPictureBox)
        Me.PanelPictureBox.Location = New System.Drawing.Point(0, 0)
        Me.PanelPictureBox.Name = "PanelPictureBox"
        Me.PanelPictureBox.Size = New System.Drawing.Size(861, 571)
        Me.PanelPictureBox.TabIndex = 1
        '
        'lblTitreFenetre
        '
        Me.lblTitreFenetre.AutoSize = True
        Me.lblTitreFenetre.Font = New System.Drawing.Font("Segoe WP", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitreFenetre.ForeColor = System.Drawing.Color.White
        Me.lblTitreFenetre.Location = New System.Drawing.Point(58, 2)
        Me.lblTitreFenetre.Name = "lblTitreFenetre"
        Me.lblTitreFenetre.Size = New System.Drawing.Size(189, 32)
        Me.lblTitreFenetre.TabIndex = 3
        Me.lblTitreFenetre.Text = "éditer la capture"
        '
        'lblRecommencer
        '
        Me.lblRecommencer.AutoSize = True
        Me.lblRecommencer.Font = New System.Drawing.Font("Segoe WP", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRecommencer.ForeColor = System.Drawing.Color.White
        Me.lblRecommencer.Location = New System.Drawing.Point(116, 114)
        Me.lblRecommencer.Name = "lblRecommencer"
        Me.lblRecommencer.Size = New System.Drawing.Size(131, 28)
        Me.lblRecommencer.TabIndex = 3
        Me.lblRecommencer.Text = "recommencer"
        '
        'lblSauvegarder
        '
        Me.lblSauvegarder.AutoSize = True
        Me.lblSauvegarder.Font = New System.Drawing.Font("Segoe WP", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSauvegarder.ForeColor = System.Drawing.Color.White
        Me.lblSauvegarder.Location = New System.Drawing.Point(128, 146)
        Me.lblSauvegarder.Name = "lblSauvegarder"
        Me.lblSauvegarder.Size = New System.Drawing.Size(119, 28)
        Me.lblSauvegarder.TabIndex = 3
        Me.lblSauvegarder.Text = "sauvegarder"
        '
        'lblPartagerTwitter
        '
        Me.lblPartagerTwitter.AutoSize = True
        Me.lblPartagerTwitter.Font = New System.Drawing.Font("Segoe WP", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPartagerTwitter.ForeColor = System.Drawing.Color.White
        Me.lblPartagerTwitter.Location = New System.Drawing.Point(64, 174)
        Me.lblPartagerTwitter.Name = "lblPartagerTwitter"
        Me.lblPartagerTwitter.Size = New System.Drawing.Size(183, 28)
        Me.lblPartagerTwitter.TabIndex = 3
        Me.lblPartagerTwitter.Text = "partager sur Twitter"
        '
        'lblSupprimer
        '
        Me.lblSupprimer.AutoSize = True
        Me.lblSupprimer.Font = New System.Drawing.Font("Segoe WP", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSupprimer.ForeColor = System.Drawing.Color.White
        Me.lblSupprimer.Location = New System.Drawing.Point(146, 312)
        Me.lblSupprimer.Name = "lblSupprimer"
        Me.lblSupprimer.Size = New System.Drawing.Size(101, 28)
        Me.lblSupprimer.TabIndex = 3
        Me.lblSupprimer.Text = "supprimer"
        '
        'PanelBoutons
        '
        Me.PanelBoutons.Controls.Add(Me.bValiderNomCapture)
        Me.PanelBoutons.Controls.Add(Me.lblNomCapture)
        Me.PanelBoutons.Controls.Add(Me.tbNomCapture)
        Me.PanelBoutons.Controls.Add(Me.lblSupprimer)
        Me.PanelBoutons.Controls.Add(Me.lblPartagerImgur)
        Me.PanelBoutons.Controls.Add(Me.lblPartagerTwitter)
        Me.PanelBoutons.Controls.Add(Me.lblSauvegarder)
        Me.PanelBoutons.Controls.Add(Me.lblTitreFenetre)
        Me.PanelBoutons.Controls.Add(Me.lblRecommencer)
        Me.PanelBoutons.Controls.Add(Me.tbUrlImgur)
        Me.PanelBoutons.Location = New System.Drawing.Point(890, 21)
        Me.PanelBoutons.Name = "PanelBoutons"
        Me.PanelBoutons.Size = New System.Drawing.Size(250, 360)
        Me.PanelBoutons.TabIndex = 4
        '
        'bValiderNomCapture
        '
        Me.bValiderNomCapture.Location = New System.Drawing.Point(23, 74)
        Me.bValiderNomCapture.Name = "bValiderNomCapture"
        Me.bValiderNomCapture.Size = New System.Drawing.Size(75, 23)
        Me.bValiderNomCapture.TabIndex = 19
        Me.bValiderNomCapture.Text = "Button1"
        Me.bValiderNomCapture.UseVisualStyleBackColor = True
        '
        'lblNomCapture
        '
        Me.lblNomCapture.Font = New System.Drawing.Font("Consolas", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNomCapture.ForeColor = System.Drawing.Color.White
        Me.lblNomCapture.Location = New System.Drawing.Point(3, 44)
        Me.lblNomCapture.Name = "lblNomCapture"
        Me.lblNomCapture.Size = New System.Drawing.Size(244, 20)
        Me.lblNomCapture.TabIndex = 18
        Me.lblNomCapture.Text = "[Nom capture]"
        Me.lblNomCapture.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tbNomCapture
        '
        Me.tbNomCapture.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNomCapture.Location = New System.Drawing.Point(3, 41)
        Me.tbNomCapture.Name = "tbNomCapture"
        Me.tbNomCapture.Size = New System.Drawing.Size(244, 26)
        Me.tbNomCapture.TabIndex = 17
        Me.tbNomCapture.Visible = False
        '
        'lblPartagerImgur
        '
        Me.lblPartagerImgur.AutoSize = True
        Me.lblPartagerImgur.Font = New System.Drawing.Font("Segoe WP", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPartagerImgur.ForeColor = System.Drawing.Color.White
        Me.lblPartagerImgur.Location = New System.Drawing.Point(71, 202)
        Me.lblPartagerImgur.Name = "lblPartagerImgur"
        Me.lblPartagerImgur.Size = New System.Drawing.Size(176, 28)
        Me.lblPartagerImgur.TabIndex = 3
        Me.lblPartagerImgur.Text = "partager sur Imgur"
        '
        'tbUrlImgur
        '
        Me.tbUrlImgur.Font = New System.Drawing.Font("Consolas", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbUrlImgur.Location = New System.Drawing.Point(3, 207)
        Me.tbUrlImgur.Name = "tbUrlImgur"
        Me.tbUrlImgur.ReadOnly = True
        Me.tbUrlImgur.Size = New System.Drawing.Size(244, 26)
        Me.tbUrlImgur.TabIndex = 16
        Me.tbUrlImgur.Visible = False
        '
        'bwImgur
        '
        '
        'Edit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1171, 583)
        Me.Controls.Add(Me.PanelBoutons)
        Me.Controls.Add(Me.PanelPictureBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Edit"
        Me.Text = "Edit"
        CType(Me.MainPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelPictureBox.ResumeLayout(False)
        Me.PanelPictureBox.PerformLayout()
        Me.PanelBoutons.ResumeLayout(False)
        Me.PanelBoutons.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MainPictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents PanelPictureBox As System.Windows.Forms.Panel
    Friend WithEvents lblTitreFenetre As System.Windows.Forms.Label
    Friend WithEvents lblRecommencer As System.Windows.Forms.Label
    Friend WithEvents lblSauvegarder As System.Windows.Forms.Label
    Friend WithEvents lblPartagerTwitter As System.Windows.Forms.Label
    Friend WithEvents lblSupprimer As System.Windows.Forms.Label
    Friend WithEvents PanelBoutons As System.Windows.Forms.Panel
    Friend WithEvents lblPartagerImgur As System.Windows.Forms.Label
    Friend WithEvents bwImgur As System.ComponentModel.BackgroundWorker
    Friend WithEvents tbUrlImgur As System.Windows.Forms.TextBox
    Friend WithEvents lblNomCapture As System.Windows.Forms.Label
    Friend WithEvents tbNomCapture As System.Windows.Forms.TextBox
    Friend WithEvents bValiderNomCapture As System.Windows.Forms.Button
End Class
