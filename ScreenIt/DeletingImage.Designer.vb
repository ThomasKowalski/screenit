﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DeletingImage
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.MainProgressBar = New System.Windows.Forms.ProgressBar()
        Me.TimerSuppression = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'MainProgressBar
        '
        Me.MainProgressBar.Location = New System.Drawing.Point(0, 0)
        Me.MainProgressBar.Name = "MainProgressBar"
        Me.MainProgressBar.Size = New System.Drawing.Size(569, 49)
        Me.MainProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee
        Me.MainProgressBar.TabIndex = 0
        '
        'TimerSuppression
        '
        Me.TimerSuppression.Interval = 5000
        '
        'DeletingImage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(572, 51)
        Me.Controls.Add(Me.MainProgressBar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "DeletingImage"
        Me.Text = "DeletingImage"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MainProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents TimerSuppression As System.Windows.Forms.Timer
End Class
