﻿Public Class DeletingImage

    Private Sub DeletingImage_Load(sender As Object, e As EventArgs) Handles Me.Load
        TimerSuppression.Enabled = True
        Me.Height = 0
        Me.Width = 0
    End Sub

    Private Sub TimerSuppression_Tick(sender As Object, e As EventArgs) Handles TimerSuppression.Tick
        Try
            My.Computer.FileSystem.DeleteFile(TempString)
        Catch ex As Exception
            Debug.Print("Exception: Unable to delete screenshot. 2")
        End Try
        Me.Close()
    End Sub
End Class