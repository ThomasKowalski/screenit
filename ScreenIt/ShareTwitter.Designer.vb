﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ShareTwitter
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tbContenuTweet = New System.Windows.Forms.TextBox()
        Me.lblSendTweet = New System.Windows.Forms.Label()
        Me.lblTitlePage = New System.Windows.Forms.Label()
        Me.PanelCapture = New System.Windows.Forms.Panel()
        Me.Previsualisation = New System.Windows.Forms.PictureBox()
        Me.BackgroundWorkerSendTweet = New System.ComponentModel.BackgroundWorker()
        Me.ProgressBarAvancement = New System.Windows.Forms.ProgressBar()
        Me.PanelCapture.SuspendLayout()
        CType(Me.Previsualisation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbContenuTweet
        '
        Me.tbContenuTweet.Font = New System.Drawing.Font("Segoe WP", 11.25!)
        Me.tbContenuTweet.Location = New System.Drawing.Point(18, 62)
        Me.tbContenuTweet.Multiline = True
        Me.tbContenuTweet.Name = "tbContenuTweet"
        Me.tbContenuTweet.Size = New System.Drawing.Size(285, 88)
        Me.tbContenuTweet.TabIndex = 12
        Me.tbContenuTweet.Text = "#Screenshot #ScreenIt"
        '
        'lblSendTweet
        '
        Me.lblSendTweet.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSendTweet.ForeColor = System.Drawing.Color.White
        Me.lblSendTweet.Location = New System.Drawing.Point(18, 153)
        Me.lblSendTweet.Name = "lblSendTweet"
        Me.lblSendTweet.Size = New System.Drawing.Size(285, 20)
        Me.lblSendTweet.TabIndex = 10
        Me.lblSendTweet.Text = "envoyer le tweet"
        Me.lblSendTweet.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTitlePage
        '
        Me.lblTitlePage.AutoSize = True
        Me.lblTitlePage.Font = New System.Drawing.Font("Segoe WP", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitlePage.ForeColor = System.Drawing.Color.White
        Me.lblTitlePage.Location = New System.Drawing.Point(12, 9)
        Me.lblTitlePage.Name = "lblTitlePage"
        Me.lblTitlePage.Size = New System.Drawing.Size(223, 32)
        Me.lblTitlePage.TabIndex = 11
        Me.lblTitlePage.Text = "partager sur Twitter"
        '
        'PanelCapture
        '
        Me.PanelCapture.Controls.Add(Me.Previsualisation)
        Me.PanelCapture.Location = New System.Drawing.Point(18, 193)
        Me.PanelCapture.Name = "PanelCapture"
        Me.PanelCapture.Size = New System.Drawing.Size(285, 158)
        Me.PanelCapture.TabIndex = 13
        '
        'Previsualisation
        '
        Me.Previsualisation.Location = New System.Drawing.Point(0, 0)
        Me.Previsualisation.Name = "Previsualisation"
        Me.Previsualisation.Size = New System.Drawing.Size(100, 50)
        Me.Previsualisation.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.Previsualisation.TabIndex = 0
        Me.Previsualisation.TabStop = False
        '
        'BackgroundWorkerSendTweet
        '
        '
        'ProgressBarAvancement
        '
        Me.ProgressBarAvancement.Location = New System.Drawing.Point(0, 56)
        Me.ProgressBarAvancement.MarqueeAnimationSpeed = 10
        Me.ProgressBarAvancement.Name = "ProgressBarAvancement"
        Me.ProgressBarAvancement.Size = New System.Drawing.Size(321, 30)
        Me.ProgressBarAvancement.Style = System.Windows.Forms.ProgressBarStyle.Marquee
        Me.ProgressBarAvancement.TabIndex = 14
        Me.ProgressBarAvancement.Visible = False
        '
        'Share
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(321, 383)
        Me.Controls.Add(Me.ProgressBarAvancement)
        Me.Controls.Add(Me.PanelCapture)
        Me.Controls.Add(Me.tbContenuTweet)
        Me.Controls.Add(Me.lblSendTweet)
        Me.Controls.Add(Me.lblTitlePage)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Share"
        Me.Text = "Share"
        Me.PanelCapture.ResumeLayout(False)
        Me.PanelCapture.PerformLayout()
        CType(Me.Previsualisation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tbContenuTweet As System.Windows.Forms.TextBox
    Friend WithEvents lblSendTweet As System.Windows.Forms.Label
    Friend WithEvents lblTitlePage As System.Windows.Forms.Label
    Friend WithEvents PanelCapture As System.Windows.Forms.Panel
    Friend WithEvents Previsualisation As System.Windows.Forms.PictureBox
    Friend WithEvents BackgroundWorkerSendTweet As System.ComponentModel.BackgroundWorker
    Friend WithEvents ProgressBarAvancement As System.Windows.Forms.ProgressBar
End Class
