﻿Public Class Visualisation

#Region "Déplacement de la fenêtre"
    Dim NewPoint As New System.Drawing.Point()
    Dim X, Y As Integer
    Dim drag As Boolean
    Dim mousex As Integer
    Dim mousey As Integer
    Sub CreateDelegates()
        For Each Ctl As System.Windows.Forms.Control In Me.Controls
            If Ctl.GetType.ToString = "System.Windows.Forms.Panel" Then
                For Each InnerCtl As System.Windows.Forms.Control In Ctl.Controls
                    If InnerCtl.GetType.ToString <> "System.Windows.Forms.TextBox" Then
                        AddHandler InnerCtl.MouseDown, AddressOf DeplacementFenetre_MouseDown
                        AddHandler InnerCtl.MouseUp, AddressOf DeplacementFenetre_MouseUp
                        AddHandler InnerCtl.MouseMove, AddressOf DeplacementFenetre_MouseMove
                    End If
                Next
            End If
            AddHandler Ctl.MouseDown, AddressOf DeplacementFenetre_MouseDown
            AddHandler Ctl.MouseUp, AddressOf DeplacementFenetre_MouseUp
            AddHandler Ctl.MouseMove, AddressOf DeplacementFenetre_MouseMove
        Next
    End Sub

    Private Sub DeplacementFenetre_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseMove
        If e.Button = Windows.Forms.MouseButtons.Left Then
            If e.Button = MouseButtons.Left Then
                If drag Then
                    Me.Top = Windows.Forms.Cursor.Position.Y - mousey
                    Me.Left = Windows.Forms.Cursor.Position.X - mousex
                End If

            End If
        End If
    End Sub
    Private Sub DeplacementFenetre_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseDown
        drag = True
        mousex = Windows.Forms.Cursor.Position.X - Me.Left
        mousey = Windows.Forms.Cursor.Position.Y - Me.Top
    End Sub
    Private Sub DeplacementFenetre_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp
        drag = False
    End Sub
#End Region

    Private ImageLocation = TempString
    Dim Image As Image
    Private Sub EditShare_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CreateDelegates()
        Image = Image.FromFile(ImageLocation)
        Previsualisation.Image = Image
        Previsualisation.SizeMode = PictureBoxSizeMode.StretchImage
        Panel1.Width = CInt(Image.Width / 2.5)
        Panel1.Height = CInt(Image.Height / 2.5)
        Previsualisation.Width = Panel1.Width
        Previsualisation.Height = Panel1.Height
        Me.Width = Panel1.Width + 200
        Me.Height = Panel1.Height
        AddHandler lblEdit.Click, AddressOf Edit.Show
        For Each Ctl In Me.Controls
            If Ctl.GetType.ToString = "System.Windows.Forms.Label" Then
                Ctl.left = Me.Width - Ctl.width
            Else
            End If
        Next
    End Sub

    Private Sub lblClose_Click(sender As Object, e As EventArgs) Handles lblClose.Click
        Image.Dispose()
        Previsualisation.Image.Dispose()
        Me.Close()
    End Sub

    Private Sub lblDelete_Click(sender As Object, e As EventArgs) Handles lblDelete.Click
        Image.Dispose()
        Previsualisation.Image.Dispose()
        DeletingImage.Show()
        Me.Close()
    End Sub

    Private Sub lblShare_Click(sender As Object, e As EventArgs) Handles lblShare.Click
        TempString = ImageLocation
        Image.Dispose()
        Previsualisation.Image.Dispose()
        ShareTwitter.Show()
        Me.Close()
    End Sub
End Class