﻿Imports System.Drawing.Drawing2D
Imports System.Net

Public Class Principal

    Dim ListeTouchesEnfoncees As New List(Of Keys)

    Dim IgnoreHotKeys = False

    Dim BlankKeys As Keys() = {Nothing, Nothing, Nothing, Nothing, Nothing}
    Dim CapturingFor As System.Windows.Forms.Label = Nothing
    Dim BackupKeys As Keys() = {Nothing, Nothing, Nothing, Nothing, Nothing}
    Dim NewKeys As Keys() = {Nothing, Nothing, Nothing, Nothing, Nothing}
    Dim RegisteringFor As Keys() = {Nothing, Nothing, Nothing, Nothing, Nothing}
    Dim OtherKeys1 As Keys() = {Nothing, Nothing, Nothing, Nothing, Nothing}
    Dim OtherKeys2 As Keys() = {Nothing, Nothing, Nothing, Nothing, Nothing}
    Dim OtherKeys3 As Keys() = {Nothing, Nothing, Nothing, Nothing, Nothing}
    Dim Compteur As Integer = 0
    Private StopHook As Boolean = False



    Private WithEvents kbHook As New KeyboardHook

#Region "Déplacement de la fenêtre"
    Dim NewPoint As New System.Drawing.Point()
    Dim X, Y As Integer
    Dim drag As Boolean
    Dim mousex As Integer
    Dim mousey As Integer
    Sub CreateDelegates()
        For Each Ctl As System.Windows.Forms.Control In Me.Controls
            If Ctl.GetType.ToString = "System.Windows.Forms.Panel" Then
                For Each InnerCtl As System.Windows.Forms.Control In Ctl.Controls
                    If InnerCtl.GetType.ToString <> "System.Windows.Forms.TextBox" Then
                        AddHandler InnerCtl.MouseDown, AddressOf DeplacementFenetre_MouseDown
                        AddHandler InnerCtl.MouseUp, AddressOf DeplacementFenetre_MouseUp
                        AddHandler InnerCtl.MouseMove, AddressOf DeplacementFenetre_MouseMove
                    End If
                Next
            End If
            AddHandler Ctl.MouseDown, AddressOf DeplacementFenetre_MouseDown
            AddHandler Ctl.MouseUp, AddressOf DeplacementFenetre_MouseUp
            AddHandler Ctl.MouseMove, AddressOf DeplacementFenetre_MouseMove
        Next
    End Sub

    Private Sub Principal_LostFocus(sender As Object, e As EventArgs) Handles Me.Deactivate
        SaveTwitterOptionsFromFields()
        tbConsumerKey.Visible = False
        tbAccessToken.Visible = False
        tbConsumerSecret.Visible = False
        tbAccessTokenSecret.Visible = False
        lblConsumerKey.Visible = True
        lblConsumerSecret.Visible = True
        lblAccessToken.Visible = True
        lblAccessTokenSecret.Visible = True
    End Sub
    Private Sub DeplacementFenetre_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseMove
        If e.Button = Windows.Forms.MouseButtons.Left Then
            If e.Button = MouseButtons.Left Then
                If drag Then
                    Me.Top = Windows.Forms.Cursor.Position.Y - mousey
                    Me.Left = Windows.Forms.Cursor.Position.X - mousex
                End If

            End If
        End If
    End Sub
    Private Sub DeplacementFenetre_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseDown
        drag = True
        mousex = Windows.Forms.Cursor.Position.X - Me.Left
        mousey = Windows.Forms.Cursor.Position.Y - Me.Top
    End Sub
    Private Sub DeplacementFenetre_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp
        drag = False
    End Sub
#End Region


    Private Sub TraitementCapture(key As Keys)
        If Not NewKeys.Contains(key) Then
            NewKeys(Compteur) = key
        End If
        Compteur += 1
        Debug.Print("new text : " & HotKeysToString(NewKeys))
        CapturingFor.Text = HotKeysToString(NewKeys)
        If Compteur > 4 Then
            ArreterCapture()
        End If
    End Sub

    Private Sub TraitementRaccourci(key As Keys)
        ListeTouchesEnfoncees.Add(key)
        If KeysScreenshot(0) <> Nothing Then
            Dim Screenshot As Boolean = True
            For Each FinalKey In KeysScreenshot
                If FinalKey <> Nothing And ListeTouchesEnfoncees.Contains(FinalKey) = False Then
                    Screenshot = False
                    Exit For
                End If
            Next
            If Screenshot Then
                Debug.Print("Taking Screenshot...")
                Dim Filename As String = RepertoireSauvegarde & "\" & ReplaceJokersInFilename(FormatNomScreenshot) & "." & FormatSauvegarde.ToLower
                TempString = Filename
                Dim Image As Image = TakeScreenshot()
                Image.Save(Filename, FormatSauvegardeImageFormat)
                Image.Dispose()
                'AddHandler IconeNotification.BalloonTipClicked, AddressOf Visualisation.Show
                If ShowNotifications Then
                    IconeNotification.ShowBalloonTip(1000, "ScreenIt!", "Capture d'écran enregistrée.", ToolTipIcon.Info)
                End If
            End If
        End If
        If KeysScreenShotEdit(0) <> Nothing Then
            Dim Screenshot As Boolean = True
            For Each FinalKey In KeysScreenShotEdit
                If FinalKey <> Nothing And ListeTouchesEnfoncees.Contains(FinalKey) = False Then
                    Screenshot = False
                    Exit For
                End If
            Next
            If Screenshot Then
                Dim Image As Image = TakeScreenshot()
                If ConserveHD Then
                    Debug.Print(RepertoireSauvegarde & "\" & ReplaceJokersInFilename(FormatNomScreenshot) & "." & FormatSauvegarde)
                    My.Computer.FileSystem.CreateDirectory(RepertoireSauvegarde & "\")
                    Image.Save(RepertoireSauvegarde & "\" & ReplaceJokersInFilename(FormatNomScreenshot) & "." & FormatSauvegarde, FormatSauvegardeImageFormat)
                End If
                Dim TempName As String = RepertoireTemporaire & "\" & ReplaceJokersInFilename(FormatNomScreenshot) & "." & FormatSauvegarde
                TempString = TempName
                Image.Save(TempName, FormatSauvegardeImageFormat)
                Edit.Show()
                ListeTouchesEnfoncees = New List(Of Keys)
            End If
        End If
        If KeysScreenshotTwitter(0) <> Nothing Then
            Dim Screenshot As Boolean = True
            For Each FinalKey In KeysScreenshotTwitter
                If FinalKey <> Nothing And ListeTouchesEnfoncees.Contains(FinalKey) = False Then
                    Screenshot = False
                    Exit For
                End If
            Next
            If Screenshot Then
                Dim Image As Image = TakeScreenshot()
                If ConserveHD Then
                    My.Computer.FileSystem.CreateDirectory(RepertoireSauvegarde & "\")
                    Image.Save(RepertoireSauvegarde & "\" & ReplaceJokersInFilename(FormatNomScreenshot) & "." & FormatSauvegarde, FormatSauvegardeImageFormat)
                End If
                Dim TempName As String = RepertoireTemporaire & "\" & ReplaceJokersInFilename(FormatNomScreenshot) & "." & FormatSauvegarde
                TempString = TempName
                Image.Save(TempName, Imaging.ImageFormat.Bmp)
                ShareTwitter.Show()
                ListeTouchesEnfoncees = New List(Of Keys)
            End If
        End If
        If KeysScreenshotImgur(0) <> Nothing Then
            Dim Screenshot As Boolean = True
            For Each FinalKey In KeysScreenshotImgur
                If FinalKey <> Nothing And ListeTouchesEnfoncees.Contains(FinalKey) = False Then
                    Screenshot = False
                    Exit For
                End If
            Next
            If Screenshot Then
                Dim Image As Image = TakeScreenshot()
                If ConserveHD Then
                    My.Computer.FileSystem.CreateDirectory(RepertoireSauvegarde & "\")
                    Image.Save(RepertoireSauvegarde & "\" & ReplaceJokersInFilename(FormatNomScreenshot) & "." & FormatSauvegarde, FormatSauvegardeImageFormat)
                End If
                Dim TempName As String = RepertoireTemporaire & "\" & ReplaceJokersInFilename(FormatNomScreenshot) & "." & FormatSauvegarde
                TempString = TempName
                Image.Save(TempName, Imaging.ImageFormat.Bmp)
                ShareTwitter.Show()
                ListeTouchesEnfoncees = New List(Of Keys)
            End If
        End If
    End Sub
    Private Sub kbHook_KeyDown(ByVal Key As Keys) Handles kbHook.KeyDown
        'Debug.Print(Key)
        'Dim temp As Integer() = {Key}
        'Debug.Print(HotKeysToString(temp))
        'Exit Sub
        If StopHook Then
            Exit Sub
        End If
        Dim Capturing As Boolean = True
        Try
            Dim CapturingForText = CapturingFor.Text
        Catch ex As Exception
            Capturing = False
        End Try
        If Capturing Then
            TraitementCapture(Key)
        Else
            TraitementRaccourci(Key)
        End If
    End Sub
    Private Sub kbHook_KeyUp(ByVal Key As Keys) Handles kbHook.KeyUp
        ListeTouchesEnfoncees.Remove(Key)
    End Sub
    Private Sub Principal_Click(sender As Object, e As EventArgs) Handles Me.Click, ongletPartage.Click, ongletQualite.Click, ongletRaccourcis.Click, ongletRepertoires.Click, panelRepertoires.Click, lblRepHQ.Click, lblRepTemp.Click, lblTitleAT.Click, lblInfosTwitter.Click
        tbLocationHQ.Visible = False
        tbLocationTemp.Visible = False
        lblLocationHQ.Visible = True
        lblLocationTemp.Visible = True
    End Sub

    Private Sub lblRepHQ_Click(sender As Object, e As EventArgs) Handles lblRepHQ.Click
        Dim Process As New Process()
        Process.Start(tbLocationHQ.Text)
    End Sub

    Private Sub Principal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            If Environment.GetCommandLineArgs(1) = "-silent" Then
                Me.Hide()
            Else
                Me.Opacity = 100
            End If
        Catch ex As Exception
            Me.Opacity = 100
        End Try
        LoadOptions()
        IconeNotification.Visible = True
        If ConserveHD Then
            cbHQ.Checked = True
        End If
        For Each panel As Panel In Me.Controls.OfType(Of Panel)()
            For Each button As Button In panel.Controls.OfType(Of Button)()
                button.Height = 0
            Next
        Next
        tbLocationHQ.Visible = False
        tbLocationTemp.Visible = False
        tbLocationHQ.Text = RepertoireSauvegarde
        tbLocationTemp.Text = RepertoireTemporaire
        lblLocationHQ.Text = RepertoireSauvegarde
        lblLocationTemp.Text = RepertoireTemporaire

        If AccessToken = "" Then
            lblAccessToken.Text = "non défini"
        Else
            lblAccessToken.Text = AccessToken
        End If
        If AccessTokenSecret = "" Then
            lblAccessTokenSecret.Text = "non défini"
        Else
            lblAccessTokenSecret.Text = AccessTokenSecret
        End If
        If ConsumerKey = "" Then
            lblConsumerKey.Text = "non défini"
        Else
            lblConsumerKey.Text = ConsumerKey
        End If
        If ConsumerSecret = "" Then
            lblConsumerSecret.Text = "non défini"
        Else
            lblConsumerSecret.Text = ConsumerSecret
        End If
        If AccessToken = "" And AccessTokenSecret = "" Then
            lblSendTweet.Text = ""
        End If
        tbAccessToken.Text = AccessToken
        tbAccessTokenSecret.Text = AccessTokenSecret
        tbConsumerKey.Text = ConsumerKey
        tbConsumerSecret.Text = ConsumerSecret

        tbNomFichier.Text = FormatNomScreenshot
        lblFilenameFormat.Text = FormatNomScreenshot
        tbNomFichier.Visible = False
        Me.Width = 540
        Me.Height = 360
        For Each Ctl In Me.Controls
            If Ctl.GetType.ToString = "System.Windows.Forms.Panel" Then
                If Ctl.Name <> "panelRepertoires" Then
                    Ctl.Visible = False
                End If
                Ctl.Location = New Point(17, 49)
            End If
        Next
        For Each Ctl In panelQualite.Controls
            If Ctl.Name.ToString.StartsWith("lblFormat") And Ctl.Text.ToString.StartsWith(FormatSauvegarde) Then
                Ctl.forecolor = Color.White
            ElseIf Ctl.Name.startswith("lblFormat") Then
                Ctl.forecolor = Color.Gray
            End If
        Next
        If KeysScreenshot(0) <> Keys.None Then
            raccourciScreenshot.Text = HotKeysToString(KeysScreenshot)
        End If
        If KeysScreenShotEdit(0) <> Keys.None Then
            raccourciEdit.Text = HotKeysToString(KeysScreenShotEdit)
        End If
        If KeysScreenshotTwitter(0) <> Keys.None Then
            raccourciTwitter.Text = HotKeysToString(KeysScreenshotTwitter)
        End If
        If KeysScreenshotImgur(0) <> Keys.None Then
            raccourciImgur.Text = HotKeysToString(KeysScreenshotImgur)
        End If
        If ShowNotifications Then
            NotificationsToolStripMenuItem.Checked = True
        End If
        CleanUpPlease()
        CreateDelegates()
    End Sub
    Private Sub ongletRepertoires_Click(sender As Object, e As EventArgs) Handles ongletRepertoires.Click, ongletQualite.Click, ongletPartage.Click, ongletRaccourcis.Click
        For Each Ctl In Me.Controls
            If Ctl.GetType.ToString = "System.Windows.Forms.Label" Then
                If Ctl.Name <> sender.Name And Ctl.name.ToString.Contains("onglet") Then
                    Ctl.forecolor = Color.Gray
                ElseIf Ctl.name = sender.name Then
                    Ctl.forecolor = Color.White
                End If
            End If
            If Ctl.GetType.ToString = "System.Windows.Forms.Panel" Then
                If Ctl.name.ToString.Contains(sender.name.ToString.Replace("onglet", "")) Then
                    Ctl.Visible = True
                Else
                    Ctl.visible = False
                End If
            End If
        Next

        If panelQualite.Visible = True Then
            TimerFilename.Enabled = True
            ActualiserResultatFilename()
        Else
            TimerFilename.Enabled = False
        End If
        Me.lblFilenameFormat.ForeColor = Color.White
    End Sub
    Private Sub raccourciScreenshot_Click(sender As Object, e As EventArgs) Handles raccourciScreenshot.Click, raccourciEdit.Click, raccourciTwitter.Click, raccourciImgur.Click
        sender.Text = "appuyez sur les touches de votre choix"
        CapturingFor = sender
        If sender.Name = "raccourciScreenshot" Then
            BackupKeys = KeysScreenshot
            RegisteringFor = KeysScreenshot
            OtherKeys1 = KeysScreenShotEdit
            OtherKeys2 = KeysScreenshotTwitter
            OtherKeys3 = KeysScreenshotImgur
        ElseIf sender.name = "raccourciEdit" Then
            BackupKeys = KeysScreenShotEdit
            RegisteringFor = KeysScreenShotEdit
            OtherKeys1 = KeysScreenshot
            OtherKeys2 = KeysScreenshotTwitter
            OtherKeys3 = KeysScreenshotImgur
        ElseIf sender.name = "raccourciTwitter" Then
            BackupKeys = KeysScreenshotTwitter
            RegisteringFor = KeysScreenshotTwitter
            OtherKeys1 = KeysScreenshot
            OtherKeys2 = KeysScreenShotEdit
            OtherKeys3 = KeysScreenshotImgur
        ElseIf sender.name = "raccourciImgur" Then
            BackupKeys = KeysScreenshotImgur
            RegisteringFor = KeysScreenshotImgur
            OtherKeys1 = KeysScreenshot
            OtherKeys2 = KeysScreenShotEdit
            OtherKeys3 = KeysScreenshotTwitter
        End If
        lblStopCapture.Visible = True
        RegisteringFor = BlankKeys
    End Sub

    Sub ArreterCapture()
        If CapturingFor.Text = "appuyez sur les touches de votre choix" Then
            CapturingFor.Text = "non défini"
        End If
        Dim TempString As String = ""
        For i = 0 To 4
            If i < 4 Then
                TempString &= NewKeys(i) & ";"
            Else
                TempString &= NewKeys(i)
            End If
        Next
        Dim ErrorDoublon1 As Boolean = True
        Dim ErrorDoublon2 As Boolean = True
        Dim ErrorDoublon3 As Boolean = True
        For Each NewlyRegisteredKey In NewKeys
            If NewlyRegisteredKey <> Keys.None Then
                If Not OtherKeys1.Contains(NewlyRegisteredKey) Or (OtherKeys1(0) = Keys.None) Then
                    ErrorDoublon1 = False
                End If
                If Not OtherKeys2.Contains(NewlyRegisteredKey) Or (OtherKeys2(0) = Keys.None) Then
                    ErrorDoublon2 = False
                End If
                If Not OtherKeys3.Contains(NewlyRegisteredKey) Or (OtherKeys3(0) = Keys.None) Then
                    ErrorDoublon3 = False
                End If
            End If
        Next
        If NewKeys(0) = Keys.None Then
            RegisteringFor = BlankKeys
            If CapturingFor.Name = "raccourciTwitter" Then
                EcrireRegistre("RaccourciTwitter", TempString)
            ElseIf CapturingFor.Name = "raccourciEdit" Then
                EcrireRegistre("RaccourciPaint", TempString)
            ElseIf CapturingFor.Name = "raccourciScreenshot" Then
                EcrireRegistre("Raccourci", TempString)
            ElseIf CapturingFor.Name = "raccourciImgur" Then
                EcrireRegistre("RaccourciImgur", TempString)
            End If
        ElseIf ErrorDoublon1 Or ErrorDoublon2 Or ErrorDoublon3 Then
            StopHook = True
            MessageBox.Show("Vous ne pouvez pas utiliser deux fois le même raccourci clavier. Merci d'en choisir un autre.", "Configuration des raccourcis claviers.", MessageBoxButtons.OK, MessageBoxIcon.Error)
            RegisteringFor = BackupKeys
            StopHook = False
        Else
            RegisteringFor = NewKeys
            If CapturingFor.Name = "raccourciTwitter" Then
                EcrireRegistre("RaccourciTwitter", TempString)
            ElseIf CapturingFor.Name = "raccourciEdit" Then
                EcrireRegistre("RaccourciPaint", TempString)
            ElseIf CapturingFor.Name = "raccourciScreenshot" Then
                EcrireRegistre("Raccourci", TempString)
            ElseIf CapturingFor.Name = "raccourciImgur" Then
                EcrireRegistre("RaccourciImgur", TempString)
            End If
        End If
        CapturingFor.Text = HotKeysToString(RegisteringFor)
        NewKeys = {Nothing, Nothing, Nothing, Nothing, Nothing}
        lblStopCapture.Visible = False
        Compteur = 0
        CapturingFor = Nothing
        OtherKeys1 = Nothing
        OtherKeys2 = Nothing
        OtherKeys3 = Nothing
        LoadOptions()
    End Sub

    Private Sub lblStopCapture_Click(sender As Object, e As EventArgs) Handles lblStopCapture.Click
        ArreterCapture()
    End Sub
    Private Sub tbNomFichier_TextChanged(sender As Object, e As EventArgs) Handles tbNomFichier.TextChanged, tbLocationHQ.TextChanged, tbLocationTemp.TextChanged
        If tbNomFichier.Text = "" Then
            TimerFilename.Enabled = False
            lblResultat.Text = "Entrez un nom de fichier."
        Else
            TimerFilename.Enabled = True
            ActualiserResultatFilename()
        End If
        EcrireRegistre("FilenameFormat", tbNomFichier.Text)
    End Sub
    Private Sub TimerFilename_Tick(sender As Object, e As EventArgs) Handles TimerFilename.Tick
        ActualiserResultatFilename()
    End Sub
    Private Sub ActualiserResultatFilename()
        If isCorrectFilename(tbNomFichier.Text) Then
            lblResultat.Text = ReplaceJokersInFilename(tbNomFichier.Text) & "." & FormatSauvegarde.ToLower
            lblResultat.ForeColor = Color.White
            bValiderFilenameFormat.Enabled = True
        Else
            lblResultat.ForeColor = Color.IndianRed
            lblResultat.Text = "Nom de fichier invalide."
            bValiderFilenameFormat.Enabled = False
        End If
    End Sub
    Private Sub lblLocationHQ_Click(sender As Object, e As EventArgs) Handles lblLocationHQ.Click
        Me.AcceptButton = Nothing
        Me.AcceptButton = bValiderAdresseHQ
        tbLocationHQ.Text = sender.Text
        tbLocationHQ.Visible = True
        sender.Visible = False
        tbLocationTemp.Visible = False
        lblLocationTemp.Visible = True
        tbLocationHQ.Focus()
    End Sub
    Private Sub lblLocationTemp_Click(sender As Object, e As EventArgs) Handles lblLocationTemp.Click
        Me.AcceptButton = Nothing
        Me.AcceptButton = bValiderTemp
        tbLocationTemp.Text = sender.Text
        tbLocationTemp.Visible = True
        sender.Visible = False
        tbLocationHQ.Visible = False
        lblLocationHQ.Visible = True
        tbLocationTemp.Focus()
    End Sub
    Private Sub bValiderAdresseHQ_Click(sender As Object, e As EventArgs) Handles bValiderAdresseHQ.Click
        tbLocationHQ.Text = FormatPath(tbLocationHQ.Text)
        If IsValidFileNameOrPath(tbLocationHQ.Text) = False Then
            tbLocationHQ.Visible = False
            lblLocationHQ.Visible = True
            Exit Sub
        End If
        IO.Directory.CreateDirectory(tbLocationHQ.Text)
        EcrireRegistre("SaveLocation", tbLocationHQ.Text)
        lblLocationHQ.Text = tbLocationHQ.Text
        tbLocationHQ.Visible = False
        lblLocationHQ.Visible = True
    End Sub
    Private Sub bValiderAdresseTemp_Click(sender As Object, e As EventArgs) Handles bValiderTemp.Click
        tbLocationTemp.Text = FormatPath(tbLocationTemp.Text)
        If IsValidFileNameOrPath(tbLocationTemp.Text) = False Then
            tbLocationTemp.Visible = False
            lblLocationTemp.Visible = True
            Exit Sub
        End If
        IO.Directory.CreateDirectory(tbLocationTemp.Text)
        EcrireRegistre("TempLocation", tbLocationTemp.Text)
        lblLocationTemp.Text = tbLocationTemp.Text
        tbLocationTemp.Visible = False
        lblLocationTemp.Visible = True
    End Sub
    Private Sub lblFormatPNG_Click(sender As Object, e As EventArgs) Handles lblFormatPNG.Click, lblFormatBMP.Click, lblFormatJPG.Click, lblFormatGIF.Click, lblFormatTIFF.Click
        For Each Ctl In panelQualite.Controls
            If Ctl.name.ToString.Contains("lblFormat") And Ctl.GetType.ToString = "System.Windows.Forms.Label" And Ctl.name.contains("BPG") = False Then
                If sender.name <> Ctl.name.ToString Then
                    Ctl.ForeColor = Color.Gray
                ElseIf Ctl.name.ToString.Contains("lblFormat") Then
                    Ctl.ForeColor = Color.White
                    TimerFilename.Enabled = True
                    ActualiserResultatFilename()
                    EcrireRegistre("Format", Ctl.Text)
                    LoadOptions()
                End If
            End If
        Next
    End Sub

    Private Sub lblFilenameFormat_Click(sender As Object, e As EventArgs) Handles lblFilenameFormat.Click
        tbNomFichier.Visible = True
        sender.visible = False
        tbNomFichier.Focus()
        Me.AcceptButton = bValiderFilenameFormat
    End Sub

    Private Sub bValiderFilenameFormat_Click(sender As Object, e As EventArgs) Handles bValiderFilenameFormat.Click
        If ReplaceJokersInFilename(tbNomFichier.Text).Contains("\") Or ReplaceJokersInFilename(tbNomFichier.Text).Contains("/") Then
            MessageBox.Show("Le nom de fichier comprend des caractères non autorisés (/ \ : ?)", "Format incorrect.", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If
        lblFilenameFormat.Text = tbNomFichier.Text
        lblFilenameFormat.Visible = True
        tbNomFichier.Visible = False
        EcrireRegistre("FilenameFormat", tbNomFichier.Text)
    End Sub

    Private Sub lblAutresParametres_Click(sender As Object, e As EventArgs) Handles lblAutresParametres.Click, lblResultat.Click, lblJokers.Click, lblJokers2.Click, lblJokers3.Click, lblSelectionFormat.Click, lblFormatBMP.Click, lblFormatGIF.Click, lblFormatJPG.Click, lblFormatPNG.Click, lblFormatTIFF.Click, panelQualite.Click
        lblFilenameFormat.Visible = True
        tbNomFichier.Text = lblFilenameFormat.Text
        tbNomFichier.Visible = False
        If sender.name.contains("lblFormat") And sender.name.endswith("lblFormat") = False Then
            lblFormatPNG_Click(sender, e)
        End If
    End Sub

    Private Sub lblSendTweet_Click(sender As Object, e As EventArgs) Handles lblSendTweet.Click
        '319631936-UIF5r3sllKnKeuzqYEJNVwY8e8ajbjMszHYPj1Dx
        'VqjYHC9SpkdJeXF2RqJSBTNm6z3QGmO9yGDdf3wtlTGYa
        If AccessToken = "" Or AccessTokenSecret = "" Then
            Exit Sub
        End If
        Dim Client As New Net.WebClient
        AddHandler Client.DownloadStringCompleted, AddressOf CompletedDownloadOfString
        Client.DownloadStringAsync(New Uri("http://thomaskowalski.net/twitterapi/tweet.php?consumerkey=" & CleAPI & "&consumersecret=" & CleAPISecret & "&accesstoken=" & AccessToken & "&accesstokensecret=" & AccessTokenSecret & "&tweet=" & System.Uri.EscapeDataString(tbContenuTweet.Text)))
    End Sub

    Private Sub CompletedDownloadOfString(sender As Object, e As DownloadStringCompletedEventArgs)
        MessageBox.Show(e.Result.ToString.Replace("<br>", vbCrLf))
    End Sub

    'La méthode ci-dessous est relativement moche, merci de ne pas m'en tenir rigueur.
    Private Sub lblAccessToken_Click(sender As Object, e As EventArgs) Handles lblAccessToken.Click, lblAccessTokenSecret.Click, lblConsumerKey.Click, lblConsumerSecret.Click
        SaveTwitterOptionsFromFields()
        If sender.name = "lblConsumerKey" Then
            tbAccessTokenSecret.Visible = False
            lblAccessTokenSecret.Visible = True
            tbAccessToken.Visible = False
            lblAccessToken.Visible = True
            tbConsumerSecret.Visible = False
            lblConsumerSecret.Visible = True
            tbConsumerKey.Text = ConsumerKey
            tbConsumerKey.Visible = True
            tbConsumerKey.Focus()
            Me.AcceptButton = bValiderConsumerKey
        ElseIf sender.name = "lblConsumerSecret" Then
            tbAccessTokenSecret.Visible = False
            lblAccessTokenSecret.Visible = True
            tbAccessToken.Visible = False
            lblAccessToken.Visible = True
            tbConsumerKey.Visible = False
            lblConsumerKey.Visible = True
            tbConsumerSecret.Text = ConsumerSecret
            tbConsumerSecret.Visible = True
            tbConsumerSecret.Focus()
            Me.AcceptButton = bValiderConsumerSecret
        ElseIf sender.name = "lblAccessToken" Then
            tbAccessTokenSecret.Visible = False
            lblAccessTokenSecret.Visible = True
            tbConsumerKey.Visible = False
            lblConsumerKey.Visible = True
            tbConsumerSecret.Visible = False
            lblConsumerSecret.Visible = True
            tbAccessToken.Text = AccessToken
            tbAccessToken.Visible = True
            tbAccessToken.Focus()
            Me.AcceptButton = bValiderAccessToken
        ElseIf sender.name = "lblAccessTokenSecret" Then
            tbConsumerSecret.Visible = False
            lblConsumerSecret.Visible = True
            tbAccessToken.Visible = False
            lblAccessToken.Visible = True
            tbConsumerKey.Visible = False
            lblConsumerKey.Visible = True
            tbAccessTokenSecret.Text = AccessTokenSecret
            tbAccessTokenSecret.Visible = True
            tbAccessTokenSecret.Focus()
            Me.AcceptButton = bValiderAccessTokenSecret
        End If
        sender.Visible = False
    End Sub

    Private Sub ShowEditShareWindow(sender As Object, e As EventArgs)
        Visualisation.Show()
    End Sub

#Region "Modification des clés d'accès Twitter"
    Private Sub bValiderAccessToken_Click(sender As Object, e As EventArgs) Handles bValiderAccessToken.Click
        EcrireRegistre("TwitterAccessToken", tbAccessToken.Text)
        tbAccessToken.Visible = False
        If tbAccessToken.Text <> "" Then
            lblAccessToken.Text = tbAccessToken.Text.Trim()
        Else
            lblAccessToken.Text = "non défini"
        End If
        lblAccessToken.Visible = True
        AccessToken = tbAccessToken.Text.Trim()
    End Sub

    Private Sub bValiderAccessTokenSecret_Click(sender As Object, e As EventArgs) Handles bValiderAccessTokenSecret.Click
        EcrireRegistre("TwitterAccessTokenSecret", tbAccessTokenSecret.Text)
        If tbAccessTokenSecret.Text <> "" Then
            lblAccessTokenSecret.Text = tbAccessTokenSecret.Text.Trim()
        Else
            lblAccessTokenSecret.Text = "non défini"
        End If
        tbAccessTokenSecret.Visible = False
        lblAccessTokenSecret.Visible = True
        AccessTokenSecret = tbAccessTokenSecret.Text.Trim()
    End Sub

    Private Sub bValiderConsumerKey_Click(sender As Object, e As EventArgs) Handles bValiderConsumerKey.Click
        EcrireRegistre("TwitterConsumerKey", tbConsumerKey.Text)
        If tbConsumerKey.Text <> "" Then
            lblConsumerKey.Text = tbConsumerKey.Text.Trim()
        Else
            lblConsumerKey.Text = "non défini"
        End If
        tbConsumerKey.Visible = False
        lblConsumerKey.Visible = True
        ConsumerKey = tbConsumerKey.Text.Trim()
    End Sub

    Private Sub bValiderConsumerSecret_Click(sender As Object, e As EventArgs) Handles bValiderConsumerSecret.Click
        EcrireRegistre("TwitterConsumerSecret", tbConsumerSecret.Text)
        If tbConsumerSecret.Text <> "" Then
            lblConsumerSecret.Text = tbConsumerSecret.Text.Trim()
        Else
            lblConsumerSecret.Text = "non défini"
        End If
        tbConsumerSecret.Visible = False
        lblConsumerSecret.Visible = True
        ConsumerSecret = tbConsumerSecret.Text.Trim()
    End Sub
#End Region

    Private Sub lblTitleATS_Click(sender As Object, e As EventArgs) Handles lblTitleCK.Click, lblTitleCS.Click, lblTitleATS.Click, lblTitleAT.Click, ongletPartage.Click, ongletQualite.Click, ongletRaccourcis.Click, ongletRepertoires.Click, panelPartage.Click, lblSendTweet.Click, tbContenuTweet.Click
        SaveTwitterOptionsFromFields()
        tbAccessToken.Visible = False
        lblAccessToken.Visible = True
        tbAccessTokenSecret.Visible = False
        lblAccessTokenSecret.Visible = True
        tbConsumerKey.Visible = False
        lblConsumerKey.Visible = True
        tbConsumerSecret.Visible = False
        lblConsumerSecret.Visible = True

    End Sub

    Private Sub IconeNotification_BalloonTipClicked(sender As Object, e As EventArgs) Handles IconeNotification.BalloonTipClicked
        Visualisation.Show()
    End Sub

    Private Sub IconeNotification_MouseClick(sender As Object, e As MouseEventArgs) Handles IconeNotification.MouseDown
        MenuNotification.Show(Cursor.Position)
    End Sub

    Private Sub IconeNotification_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles IconeNotification.MouseDoubleClick
        afficherfenetre()
    End Sub

    Private Sub lblFermer_Click(sender As Object, e As EventArgs) Handles lblFermer.Click
        Me.Hide()
    End Sub

    Private Sub lblFormatBPG_Click(sender As Object, e As EventArgs) Handles lblFormatBPG.Click
        MessageBox.Show("Désolé, mais le format BPG n'est pas *encore* pris en charge par ScreenIt! Il le sera bientôt !", "Format non pris en charge.", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    End Sub

    Private Sub lblConsumerSecret_Click(sender As Object, e As EventArgs) Handles lblConsumerSecret.Click
        lblConsumerSecret.Visible = False
        tbConsumerSecret.Visible = True
        tbConsumerSecret.Focus()
    End Sub

    Private Sub lblConsumerKey_Click(sender As Object, e As EventArgs) Handles lblConsumerKey.Click
        lblConsumerKey.Visible = False
        tbConsumerKey.Visible = True
        tbConsumerSecret.Focus()
    End Sub

    Private Sub AfficherLaFenêtrePrincipaleToolStripMenuItem_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub QuitterToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles QuitterToolStripMenuItem.Click
        Dim ResultatMB = MessageBox.Show("Voulez-vous vraiment quitter ScreenIt! ? " & vbCrLf & "Vous ne pourrez plus prendre de captures d'écran.", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If ResultatMB = Windows.Forms.DialogResult.Yes Then
            End
        End If
    End Sub

    Private Sub AfficherFenetre()
        Me.Show()
        Me.Opacity = 100
        Me.Visible = True
        Me.TopMost = True
        Me.TopMost = False
    End Sub

    Private Sub RéglagesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RéglagesToolStripMenuItem.Click
        AfficherFenetre()
    End Sub

    Private Sub NotificationsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NotificationsToolStripMenuItem.Click
        ShowNotifications = Not ShowNotifications
        sender.Checked = Not sender.checked
        EcrireRegistre("Notifications", ShowNotifications)
    End Sub

    Private Function FormatPath(Path As String) As String
        Path = Path.Replace("/", "\")
        While Path.Contains("\\")
            Path = Path.Replace("\\", "\")
        End While
        Path = Path.Substring(0, 1).ToUpper & Path.Substring(1, Path.Length - 1)
        Return Path
    End Function

    Private Sub SaveTwitterOptionsFromFields()
        lblConsumerKey.Text = tbConsumerKey.Text.Trim
        lblConsumerSecret.Text = tbConsumerSecret.Text.Trim
        lblAccessToken.Text = tbAccessToken.Text.Trim
        lblAccessTokenSecret.Text = tbAccessTokenSecret.Text.Trim
        ConsumerKey = tbConsumerKey.Text.Trim
        ConsumerSecret = tbConsumerSecret.Text.Trim
        AccessToken = tbAccessToken.Text.Trim
        AccessTokenSecret = tbAccessTokenSecret.Text.Trim
        EcrireRegistre("TwitterConsumerKey", ConsumerKey)
        EcrireRegistre("TwitterConsumerSecret", ConsumerSecret)
        EcrireRegistre("TwitterAccessToken", AccessToken)
        EcrireRegistre("TwitterAccessTokenSecret", AccessTokenSecret)
    End Sub
End Class
