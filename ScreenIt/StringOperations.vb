﻿Module StringOperations
    Public Function ReplaceJokersInFilename(Filename As String) As String
        Filename = Filename.Replace("\a", Date.Now.Year)
        Filename = Filename.Replace("\y", Date.Now.Year)
        Filename = Filename.Replace("\mn", Date.Now.Minute)
        Filename = Filename.Replace("\m", Date.Now.Month)
        Filename = Filename.Replace("\d", Date.Now.Day)
        Filename = Filename.Replace("\j", Date.Now.Day)
        Filename = Filename.Replace("\s", Date.Now.Second)
        Filename = Filename.Replace("\h", Date.Now.Hour)
        Return Filename
    End Function

    Public Function isCorrectFilename(Filename As String) As Boolean
        Filename = ReplaceJokersInFilename(Filename)
        If Filename.Contains("/") Or Filename.Contains("\") Or Filename.Contains(":") Or Filename.Contains("?") Then
            Return False
        Else
            Return True
        End If
    End Function

    Function IsValidFileNameOrPath(ByVal name As String) As Boolean
        If name Is Nothing Or name = "" Then
            Return False
            Exit Function
        End If
        For Each badChar As Char In System.IO.Path.GetInvalidPathChars
            If InStr(name, badChar) > 0 Then
                Return False
                Exit Function
            End If
        Next
        If name <> "" Then
            If name.Substring(1, 2) <> ":\" And name.Substring(1, 2) <> ":/" Then
                Return False
                Exit Function
            End If
        End If
        Try
            IO.Directory.CreateDirectory(name)
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Function HotKeysToString(HotKeys As Keys())
        Dim Result As String = ""
        Dim Compteur As Integer = 0
        Dim ArrayHumanKeys As String() = {Nothing, Nothing, Nothing, Nothing, Nothing}
        For i = 0 To HotKeys.Count - 1
            Dim Number As Integer = CInt(HotKeys(i))
            If Number = 0 Then : Exit For
            End If
            Dim ThisKey As String = ""
            Select Case Number
                Case 162
                    ThisKey = "Ctrl (G)"
                Case 91
                    ThisKey = "Win. (G)"
                Case 164
                    ThisKey = "Alt"
                Case 165
                    ThisKey = "Alt Gr."
                Case 163
                    ThisKey = "Ctrl (D)"
                Case 92
                    ThisKey = "Win. (D)"
                Case 13
                    ThisKey = "Enter"
                Case 8
                    ThisKey = "Ret. Arr."
                Case 160
                    ThisKey = "Maj."
                Case 20
                    ThisKey = "Verr. Maj."
                Case 9
                    ThisKey = "Tab."
                Case 27
                    ThisKey = "Esc."
                Case 44
                    ThisKey = "Imp. Ecr."
                Case 36
                    ThisKey = "Orig."
                Case 35
                    ThisKey = "Fin"
                Case 33
                    ThisKey = "Déb. Page"
                Case 34
                    ThisKey = "Fin Page"
                Case 144
                    ThisKey = "Verr. Num."
                Case 111
                    ThisKey = "/"
                Case 106
                    ThisKey = "*"
                Case 109
                    ThisKey = "-"
                Case 107
                    ThisKey = "+"
                Case 110
                    ThisKey = "."
                Case 32
                    ThisKey = "Espace"
                Case 93
                    ThisKey = "Menu"
                Case 37
                    ThisKey = "Gauche"
                Case 38
                    ThisKey = "Haut"
                Case 39
                    ThisKey = "Droite"
                Case 40
                    ThisKey = "Bas"
                Case 12
                    ThisKey = "5"
                Case Else
                    ThisKey = Chr(Number)
            End Select
            If Number >= 112 And Number <= 123 Then
                ThisKey = "F" & Number - 111
            End If
            If Number >= 96 And Number <= 105 Then
                ThisKey = Number - 96
            End If
            ArrayHumanKeys(Compteur) = ThisKey
            compteur += 1
        Next
        For i = 0 To ArrayHumanKeys.Count - 1
            If ArrayHumanKeys(i) = Nothing Then
                Exit For
            End If
            If ArrayHumanKeys(i).Contains("Ctrl") And i <> 0 Then
                Dim TempString = ArrayHumanKeys(0)
                ArrayHumanKeys(0) = ArrayHumanKeys(i)
                ArrayHumanKeys(i) = TempString
            End If
            If ArrayHumanKeys(i).Contains("Alt") And i <> 0 And i <> 1 Then
                Dim TempString = ArrayHumanKeys(1)
                ArrayHumanKeys(1) = ArrayHumanKeys(i)
                ArrayHumanKeys(i) = TempString
            End If
        Next
        Result = Nothing
        For i = 0 To ArrayHumanKeys.Count - 1
            If ArrayHumanKeys(i) = Nothing Then
                Exit For
            End If
            Result &= ArrayHumanKeys(i) & " + "
        Next
        If Result = "" Then
            Return "non défini"
        Else
            Return Result.Substring(0, Result.Length - 3)
        End If
    End Function

    Public Function ASCIIBytesToString(ByVal bytes() As Byte) As String
        Return System.Text.Encoding.ASCII.GetString(bytes)
    End Function
End Module
